import React, { useEffect, useState } from "react";
import {
    CCard,
    CCardBody,
    CCardHeader,
    CCardFooter,
    CCol,
    CRow,
    CButton,
    CLabel,
    CInput
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Tables from "./Tables";
import ApiTranRedeem from '../../api/ApiTranRedeem';
import { useDispatch, useSelector } from "react-redux";
import ApiTranLogBack from '../../api/ApiTranLogBack';

const TranRedeem = () => {
    const [dataTable, setDataTable] = useState([]);
    const [rawDataTable, setRawDataTable] = useState([]);
    const [empid, setEmpId] = useState("");
    const [username, setUsername] = useState("");
    const [userfullname, setUserfullname] = useState("");
    const [email, setEmail] = useState("");
    const userState = useSelector((state) => state.user);

    useEffect(() => {
        getData();
        return () => {
        }
    }, []);


    const getData = async () => {
        try {
            var result = await ApiTranRedeem.getAllTranRedeem();
            if (result.status === 200) {
                const { data } = result.data;
                setRawDataTable(data);
                for (let i = 0; i < data.length; i++) {
                    data[i].order = i + 1
                }
                setDataTable(data);
            }
        } catch (error) {

        }
    }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b>อนุมัติของรางวัล</b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    );
};

export default TranRedeem;
