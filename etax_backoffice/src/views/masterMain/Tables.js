import React, { useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CTextarea,
    CBadge
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import ApiMasterConfig from '../../api/ApiMasterConfig';
import Swal from "sweetalert2";
import parse from 'html-react-parser';
import moment from 'moment';
import DatePicker from "react-datepicker";
import ReactQuill from "react-quill";
import "quill/dist/quill.snow.css";
import "react-datepicker/dist/react-datepicker.css";
import ApiMasterRedeem from '../../api/ApiMasterRedeem';
import { CheckFile } from "../../utils/uploadfile";
import { WEB_API } from "../../env";
import ApiMasterCampaign from '../../api/ApiMasterCampaign';
import ApiEtaxMain from '../../api/ApiEtaxMain';
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const DemoTable = ({ data = [], refreshData = () => { } }) => {

    const modules = {
        toolbar: [
            ["bold", "italic", "underline", "strike"], // toggled buttons
            ["blockquote", "code-block"],
            [{ header: 1 }, { header: 2 }], // custom button values
            [{ list: "ordered" }, { list: "bullet" }],
            [{ script: "sub" }, { script: "super" }], // superscript/subscript
            [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
            [{ direction: "rtl" }], // text direction
            [{ size: ["small", false, "large", "huge"] }], // custom dropdown
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
            [{ color: [] }, { background: [] }], // dropdown with defaults from theme
            [{ font: [] }],
            [{ align: [] }],
            ["clean"], // remove formatting button
        ],
    };

    const [modalEdit, setModalEdit] = useState(false);
    const [modalNew, setModalNew] = useState(false);
    const [modalConfirm, setModalConfirm] = useState(false);

    // const [title, setTitle] = useState('')
    // const [description, setDescription] = useState('')
    // const [dataSelected, setDataSelected] = useState('');
    // const [picture, setPicture] = useState('')
    // const [showImage, setShowImage] = useState('')

    const [invoiceNum, setInvoiceNum] = useState('')
    const [taxId, setTaxId] = useState('')
    const [dataSelected, setDataSelected] = useState('')
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')

    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());

    const toggle = () => {
        setModalEdit(!modalEdit);
    }

    const toggleNew = () => {
        setModalNew(!modalNew);
    }

    const toggleConfirm = () => {
        setModalConfirm(!modalConfirm);
    }

    // const func_attachFile = async (e) => {
    //     if (e.files.length > 0) {
    //         var file = e.files[0];

    //         var type = ["image/png", "image/jpg", "image/jpeg"];
    //         var message = "";
    //         var check = CheckFile({
    //             file,
    //             size: 10,
    //             type: type,
    //             message,
    //         });
    //         if (check) {
    //             setPicture(file)
    //         }
    //     }
    // };

    // const newDetails = () => {
    //     setTitle('')
    //     setDescription('')
    //     setStartDate(new Date())
    //     setEndDate(new Date())
    //     setPicture('')
    //     toggleNew();
    // }

    // const onSubmitNew = (e) => {
    //     e.preventDefault();
    //     var formdata = new FormData();
    //     formdata.append("title", title);
    //     formdata.append("description", description);
    //     formdata.append("start_date", startDate.toISOString());
    //     formdata.append("end_date", endDate.toISOString());
    //     formdata.append("createby", 0);
    //     formdata.append("updateby", 0);
    //     formdata.append("file", picture);
    //     newDataMtCampaign(formdata);
    // }

    const deleteDetails = (record) => {
        toggleConfirm();
        setDataSelected(record);
    }

    const editDetails = (record) => {
        toggle();
        setDataSelected(record);
        setInvoiceNum(record.invoice_num)
        setTaxId(record.tax_id)
        setName(record.name)
        setEmail(record.email)
    }

    const onSubmitEdit = (e) => {

        e.preventDefault();
        const data = {
            "id": dataSelected.id,
            "name": name,
            "email": email,
            "invoice_num": invoiceNum,
            "tax_id": taxId
        }
        updateEtax(data)
    }

    const updateEtax = async (data) => {
        try {
            const result = await ApiEtaxMain.updateExMain(data);
            if (result.status === 200) {
                const { data } = result.data;
                toggle();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            console.log(error.response)
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });

        }
    }


    // const onDelete = async () => {
    //     try {
    //         const id = dataSelected.id
    //         const result = await ApiEtaxMain.deleteExMain(id);
    //         if (result.status === 200) {
    //             const { data } = result.data;
    //             toggleConfirm();
    //             Swal.fire({
    //                 icon: "success",
    //                 title: "ลบสำเร็จ",
    //                 timer: 2000,
    //             }).then((success) => {
    //                 refreshData();
    //             });
    //         }
    //     } catch (error) {
    //         Swal.fire({
    //             icon: "error",
    //             title: error.response.data,
    //         });
    //     }
    // }



    const fields = [
        { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
        {
            key: 'option',
            label: '',
            _style: { width: '1%' },
            filter: false,
        },
        { key: 'invoice_num', label: "เลขใบกำกับภาษี", _style: { width: '16%' } },
        { key: 'tax_id', label: "เลขประจำตัวผู้เสียภาษี", _style: { width: '16%' } },
        { key: 'name', label: "ชื่อ", _style: { width: '16%' } },
        { key: 'email', label: "อีเมล", _style: { width: '16%' } },
        { key: 'subtotal', label: "จำนวนเงินที่ต้องชำระ(บาท)", _style: { width: '16%' } },
        { key: 'paystatus', label: "สถานะการชำระเงิน", _style: { width: '16%' } },

    ]

    return (
        <>
            <CDataTable
                items={data}
                fields={fields}
                tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                    'order':
                        (item) => (
                            <td style={{ textAlign: 'center' }}>
                                {item.order}
                            </td>
                        ),
                    'option':
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton
                                        color="primary-custom"
                                        variant="reverse"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { editDetails(item) }}
                                        onMouseOver={(e) => document.getElementById(`edit-${item.order}`).src = edit_hover}
                                        onMouseOut={(e) => document.getElementById(`edit-${item.order}`).src = edit_icon}
                                    >
                                        <img id={`edit-${item.order}`} src={edit_icon} />
                                    </CButton>
                                    {/* <CButton
                                        className="ml-3"
                                        color="danger-custom"
                                        variant="reverse"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { deleteDetails(item) }}
                                        onMouseOver={(e) => document.getElementById(`delete-${item.order}`).src = trash_hover}
                                        onMouseOut={(e) => document.getElementById(`delete-${item.order}`).src = trash_icon}
                                    >
                                        <img id={`delete-${item.order}`} src={trash_icon} />
                                    </CButton> */}
                                </CButtonGroup>
                            </td>
                        ),
                    'paystatus':
                        (item) => (
                            <td>{item.paystatus === 'A' && <CBadge className="mr-1" color="success">ชำระเงินแล้ว</CBadge> || item.paystatus === 'I' && <CBadge className="mr-1" color="danger">ยังไม่ได้ชำระเงิน</CBadge> || item.paystatus === 'O' && <CBadge className="mr-1" color="warning">ยอดชำระเกิน</CBadge> || item.paystatus === 'U' && <CBadge className="mr-1" color="warning">ยอดชำระขาด</CBadge>} </td>
                        ),

                }}
            />

            <CModal
                show={modalConfirm}
                onClose={setModalConfirm}
                color="danger"
            >
                <CModalHeader closeButton>
                    <CModalTitle>คืนเงิน</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    คุณต้องการจะคืนเงินจำนวน ...... ?
                </CModalBody>
                <CModalFooter>
                    <CButton color="primary">ยืนยัน</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalConfirm(false)}
                    >ยกเลิก</CButton>
                </CModalFooter>
            </CModal>

            <CModal
                show={modalEdit}
                onClose={toggle}
                style={{ width: '133%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>แก้ไขข้อมูล</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmitEdit} action="" >
                    <CModalBody>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> เลขใบกำกับภาษี <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={(e) => { setInvoiceNum(e.target.value) }} value={invoiceNum} id="code" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> เลขประจำตัวผู้เสียภาษี <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={(e) => { setTaxId(e.target.value) }} value={taxId} id="code" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> ชื่อ <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={(e) => { setName(e.target.value) }} value={name} id="code" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> อีเมล <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={(e) => { setEmail(e.target.value) }} value={email} id="code" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >
        </>
    )
}

export default DemoTable
