import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton,
    CLabel,
    CInput
} from '@coreui/react'
import CIcon from "@coreui/icons-react";
import Tables from './Tables';
import ApiTranLogBack from '../../api/ApiTranLogBack';
import ApiMasterRedeem from '../../api/ApiMasterRedeem';
import ApiMasterCampaign from '../../api/ApiMasterCampaign';
import ApiEtaxGenInvoice from '../../api/ApiEtaxGenInvoice';

const MasterCampaign = () => {
    const [dataTable, setDataTable] = useState([]);
    const [rawDataTable, setRawDataTable] = useState([]);
    const [value, setValue] = useState("");
    const [code, setCode] = useState("");
    const [name, setName] = useState("");
    const [point, setPoint] = useState("");

    useEffect(() => {
        getData();
        return () => {
        }
    }, []);

    const getData = async () => {
        try {
            const result = await ApiEtaxGenInvoice.getAllExGenInvoice();
            if (result.status === 200) {
                const { data } = result.data;
                setRawDataTable(data);
                for (let i = 0; i < data.length; i++) {
                    data[i].order = i+1
                }
                setDataTable(data);
            }
        } catch (error) {

        }
    }

    // const onSearch = async () => {
    //     var model = {
    //         activity_detail: "Menu Config Search",
    //         json: JSON.stringify({
    //             name: name,
    //             point: point
    //         })
    //     }
    //     await ApiTranLogBack.addlog(model);
    //     if(rawDataTable && rawDataTable.length > 0){
    //         var result = rawDataTable;
    //         if(name && name.length > 0){
    //             result = result.filter((x) => x.name.toLowerCase().includes(name.toLowerCase()));
    //         }
    //         if(point && point.length > 0){
    //             result = result.filter((x) => x.point.toString().toLowerCase().includes(point.toLowerCase()));
    //         }
    //         for (let i = 0; i < result.length; i++) {
    //             result[i].order = i+1
    //         }
    //         setDataTable(result);
    //     }
    // }

    // const resetSearchOption = () => {
    //     setName("");
    //     setPoint("");
    //     var result = rawDataTable;
    //     for (let i = 0; i < result.length; i++) {
    //         result[i].order = i+1
    //     }
    //     setDataTable(result);
    // }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> การจัดการออกใบกำกับภาษี </b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default MasterCampaign