import React, { useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CTextarea
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import ApiMasterConfig from '../../api/ApiMasterConfig';
import Swal from "sweetalert2";
import parse from 'html-react-parser';
import moment from 'moment';
import DatePicker from "react-datepicker";
import ReactQuill from "react-quill";
import "quill/dist/quill.snow.css";
import "react-datepicker/dist/react-datepicker.css";
import ApiMasterRedeem from '../../api/ApiMasterRedeem';
import { CheckFile } from "../../utils/uploadfile";
import { WEB_API } from "../../env";
import ApiMasterCampaign from '../../api/ApiMasterCampaign';
import ApiEtaxGenInvoice from '../../api/ApiEtaxGenInvoice';
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const DemoTable = ({ data = [], refreshData = () => { } }) => {

    const modules = {
        toolbar: [
            ["bold", "italic", "underline", "strike"], // toggled buttons
            ["blockquote", "code-block"],
            [{ header: 1 }, { header: 2 }], // custom button values
            [{ list: "ordered" }, { list: "bullet" }],
            [{ script: "sub" }, { script: "super" }], // superscript/subscript
            [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
            [{ direction: "rtl" }], // text direction
            [{ size: ["small", false, "large", "huge"] }], // custom dropdown
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
            [{ color: [] }, { background: [] }], // dropdown with defaults from theme
            [{ font: [] }],
            [{ align: [] }],
            ["clean"], // remove formatting button
        ],
    };

    const [modalEdit, setModalEdit] = useState(false);
    const [modalNew, setModalNew] = useState(false);
    const [modalConfirm, setModalConfirm] = useState(false);

    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [dataSelected, setDataSelected] = useState('');
    const [picture, setPicture] = useState('')
    const [showImage, setShowImage] = useState('')

    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());

    const toggle = () => {
        setModalEdit(!modalEdit);
    }

    const toggleNew = () => {
        setModalNew(!modalNew);
    }

    const toggleConfirm = () => {
        setModalConfirm(!modalConfirm);
    }


    const onDelete = async () => {
        try {
            const id = dataSelected.id
            const result = await ApiMasterCampaign.deleteMtCampaign(id);
            if (result.status === 200) {
                const { data } = result.data;
                toggleConfirm();
                Swal.fire({
                    icon: "success",
                    title: "ลบสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const onClickgenInvoicePdf = (record) => {

        const data = {
            "id": record.id,
            "name": record.name,
            "invoice_num": record.invoice_num,
            "address": record.address,
            "province": record.province,
            "district": record.district,
            "sub_district": record.sub_district,
            "postal_code": record.postal_code,
            "email": record.email,
            "date": record.date,
            "tax_id": record.tax_id,
            "total": record.total,
            "taxrate": record.taxrate,
            "subtotal": record.subtotal,
            "product_name": record.product_name
        }
        onGenPdf(data)
    }

    const onGenPdf = async (data) => {
        try {
            Swal.showLoading()
            const result = await ApiEtaxGenInvoice.genPdf(data);
            if (result.status === 200) {
                const { data } = result.data;
                Swal.fire({
                    icon: "success",
                    title: "ออกใบกำกับภาษีสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    window.location.reload();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }


    const fields = [
        { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
        {
            key: 'option',
            label: '',
            _style: { width: '10%' },
            filter: false,
        },
        { key: 'invoice_num', label: "เลขใบกำกับภาษี", _style: { width: '16%' } },
        { key: 'name', label: "ชื่อ", _style: { width: '16%' } },
        { key: 'date', label: "วันที่ชำระเงิน", _style: { width: '16%' } },
        { key: 'paystatus', label: "สถานะการชำระเงิน", _style: { width: '16%' } },

    ]

    return (
        <>
            <CDataTable
                items={data}
                fields={fields}
                tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                    'order':
                        (item) => (
                            <td style={{ textAlign: 'center' }}>
                                {item.order}
                            </td>
                        ),
                    'option':
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton
                                        className="btn-primary"
                                        onClick={() => { onClickgenInvoicePdf(item) }}
                                    >
                                        ออกใบกำกับภาษี
                                    </CButton>
                                </CButtonGroup>
                            </td>
                        ),
                    'date':
                        (item) => (
                            <td>
                                {moment(item.date)
                                    // .add(543, "years")
                                    .format("DD-MM-YYYY / HH:mm")}
                            </td>
                        ),
                    'paystatus':
                        (item) => (
                            <td>{item.paystatus === 'A' ? "ชำระเงินแล้ว" : "ยังไม่ได้ชำระเงิน"}</td>
                        ),
                }}
            />

            <CModal
                show={modalConfirm}
                onClose={setModalConfirm}
                color="danger"
            >
                <CModalHeader closeButton>
                    <CModalTitle>ลบรายการ</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    คุณต้องการที่จะลบรายการนี้ ?
                </CModalBody>
                <CModalFooter>
                    <CButton onClick={onDelete} color="primary">ยืนยัน</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalConfirm(false)}
                    >ยกเลิก</CButton>
                </CModalFooter>
            </CModal>
            <CModal
                show={modalEdit}
                onClose={toggle}
                style={{ width: '133%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>แก้ไขข้อมูล</CModalTitle>
                </CModalHeader>
                <CForm action="" >
                    <CModalBody>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> ชื่อ <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setTitle(e.target.value)} value={title} id="code" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6" >
                                        <CFormGroup >
                                            <CLabel htmlFor="value">วันที่เริ่ม<span style={{ color: 'red' }}>*</span></CLabel>
                                            <DatePicker
                                                selected={startDate}
                                                onChange={(date) => setStartDate(date)}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                className="w-100 border rounded pl-2"
                                            />
                                        </CFormGroup >
                                    </CCol>
                                    <CCol xs="6">
                                        <CFormGroup >
                                            <CLabel htmlFor="value">วันที่สิ้นสุด<span style={{ color: 'red' }}>*</span></CLabel>
                                            <DatePicker
                                                selected={endDate}
                                                onChange={(date) => setEndDate(date)}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                className="w-100 border rounded pl-2"
                                            />
                                        </CFormGroup >
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="description"> รายละเอียด <span style={{ color: 'red' }}>*</span></CLabel>
                                            <ReactQuill onChange={(e) => setDescription(e)} modules={modules} value={description} />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="image">อัพโหลดรูปภาพ<span style={{ color: 'red' }}>*</span></CLabel>
                                            <input type="file" id="myFile" />
                                            {
                                                picture && <img className="mt-2" style={{ width: "100%" }} src={URL.createObjectURL(picture)}></img>
                                            }
                                            {
                                                (showImage && !picture) && <img className="mt-2" style={{ width: "100%" }} src={WEB_API + showImage}></img>
                                            }
                                            {/* <img className = "mt-2" style={{ height: "100px" }} src={}></img> */}
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >

            <CModal
                show={modalNew}
                onClose={toggleNew}
                style={{ width: '133%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>เพิ่มข้อมูล</CModalTitle>
                </CModalHeader>
                <CForm action="" >
                    <CModalBody >
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> หัวข้อ <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={e => setTitle(e.target.value)} value={title} id="name" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6" >
                                        <CFormGroup >
                                            <CLabel htmlFor="value">วันที่เริ่ม<span style={{ color: 'red' }}>*</span></CLabel>
                                            <DatePicker
                                                selected={startDate}
                                                onChange={(date) => setStartDate(date)}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                className="w-100 border rounded pl-2"
                                            />
                                        </CFormGroup >
                                    </CCol>
                                    <CCol xs="6">
                                        <CFormGroup >
                                            <CLabel htmlFor="value">วันที่สิ้นสุด<span style={{ color: 'red' }}>*</span></CLabel>
                                            <DatePicker
                                                selected={endDate}
                                                onChange={(date) => setEndDate(date)}
                                                showTimeSelect
                                                timeFormat="HH:mm"
                                                timeIntervals={15}
                                                timeCaption="time"
                                                dateFormat="MMMM d, yyyy h:mm aa"
                                                className="w-100 border rounded pl-2"
                                            />
                                        </CFormGroup >
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="description"> รายละเอียด <span style={{ color: 'red' }}>*</span></CLabel>
                                            <ReactQuill onChange={(e) => setDescription(e)} modules={modules} value={description} />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="6">
                                        <CFormGroup>
                                            <CLabel htmlFor="image">อัพโหลดรูปภาพ<span style={{ color: 'red' }}>*</span></CLabel>
                                            <input type="file" id="picture" />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggleNew}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >
        </>
    )
}

export default DemoTable
