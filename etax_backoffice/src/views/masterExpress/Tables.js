import React, { useState, useEffect } from "react";
import { WEB_API } from "../../env";
import {
    CCardBody,
    CTextarea,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CRow,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup,
    CInputCheckbox,
    CPopover,
    CLink,
    CSwitch,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import ApiUser from '../../api/ApiUser';
import ApiTranRedeem from '../../api/ApiTranRedeem'
import ApiDownloadFilePdf from '../../api/ApiDownloadFilePdf'
import ApiSendEmail from '../../api/ApiSendEmail'
import Swal from "sweetalert2";
import moment from 'moment';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");
const pdficon = require("./../../images/projectImage/Vector.png");


const Tables = ({ data = [], refreshData = () => { } }) => {
    const history = useHistory();
    const [modal, setModal] = useState(false);
    const [statusRedeem, setStatusRedeem] = useState([]);
    const [modalConfirm, setModalConfirm] = useState(false);
    const [modalConfirm2, setModalConfirm2] = useState(false);

    const [unapproveId, setUnapproveId] = useState('');
    const [note, setNote] = useState('');


    useEffect(() => {
        return () => {
        }
    }, []);


    const toggle = () => {
        setModal(!modal);
    };

    const dataMockUp = [
        {
            "order": 1,
            "invoice_num": "123E8156",
            "name": "Kittanon",
            "email": "Kittanon@gmail.com",
            "createdate": "22/11/2564 16:34",
        },
        {
            "order": 2,
            "invoice_num": "123E8156",
            "name": "Dez",
            "email": "dez@gmail.com",
            "createdate": "22/11/2564 16:34",
        },
        {
            "order": 3,
            "invoice_num": "123E8156",
            "name": "Poom",
            "email": "Poom@gmail.com",
            "createdate": "22/11/2564 16:34",
        },
        {
            "order": 4,
            "invoice_num": "123E8156",
            "name": "Chiab",
            "email": "Chiab@gmail.com",
            "createdate": "22/11/2564 16:34",
        },
        {
            "order": 5,
            "invoice_num": "123E8156",
            "name": "Earn",
            "email": "Earn@gmail.com",
            "createdate": "22/11/2564 16:34",
        },
    ]

    const fields = [
        { key: "order", label: "ลำดับ", _style: { width: "1%" } },
        {
            key: 'option',
            label: '',
            _style: { width: '1%' },
            filter: false,
        },
        { key: "invoice_num", label: "เลขใบกำกับภาษี", _style: { minWidth: "150px" } },
        { key: "name", label: "ชื่อ", _style: { minWidth: "150px" } },
        { key: "email", label: "อีเมล", _style: { minWidth: "150px" } },
        { key: "createdate", label: "วันที่สร้าง", _style: { minWidth: "150px" } },
        { key: "filepdf", label: "ไฟล์(PDF)", _style: { minWidth: "150px" } },
    ];


    const checkData = (e, item) => {
        let value = e.target.checked
        let arr = statusRedeem
        if (value) {
            arr.push(item.id)
        }
        else {
            let index = arr.indexOf(item.id)
            arr.splice(index, 1)
        }
        console.log(statusRedeem)
    }

    const redeemFor = () => {
        if (statusRedeem.length === 0) {
            console.log(statusRedeem.length)
            Swal.fire({
                icon: "error",
                title: "กรุณาเลือกข้อมูล",
            });
        }
        else {
            updateRedeemStatus(statusRedeem, 1)
        }
    }

    const updateRedeemStatus = async (status, update) => {
        try {
            const result = await ApiTranRedeem.updateRedeemStatus(status, update);
            if (result.status === 200) {
                toggleConfirm();
                Swal.fire({
                    icon: "success",
                    title: "แก้ไขข้อมูลสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    window.location.reload();
                });
            }
            return result
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const sendEmail = async (data) => {
        try {
            Swal.showLoading()
            const result = await ApiSendEmail.sendEmail(data);
            if (result.status === 200) {
                Swal.fire({
                    icon: "success",
                    title: "ส่งอีเมลสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    window.location.reload();
                });
            }
            return result
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data.data,
            });
        }
    }

    const toggleConfirm = () => {
        setModalConfirm(!modalConfirm);
    }

    const toggleConfirm2 = () => {
        setModalConfirm2(!modalConfirm2);
    }

    const onUnapprove = (record) => {
        toggleConfirm2()
        setUnapproveId(record.id)
    }

    const unapprove = async () => {
        const model = {
            "id": unapproveId,
            "note": note,
            "updateby": 1
        }
        try {
            const result = await ApiTranRedeem.unapproveRedeemStatus(model);
            if (result.status === 200) {
                toggleConfirm2();
                Swal.fire({
                    icon: "success",
                    title: "แก้ไขข้อมูลสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    window.location.reload();
                });
            }
            return result
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const downloadFilePdf = async (data) => {

        const model = {
            file: data.file
        }
        console.log(model)
        try {
            const result = await ApiDownloadFilePdf.downloadFilePdf(model);
            if (result.status === 200) {
                console.log("download is success")
            }
            return result
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    return (
        <>
            <CDataTable
                items={data}
                fields={fields}
                tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                    'order':
                        (item) => (
                            <td style={{ textAlign: 'center' }}>
                                {item.order}
                            </td>
                        ),
                    'option':
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton
                                        className="btn-primary"
                                        onClick={() => sendEmail(item)}
                                    >
                                        ส่ง
                                    </CButton>
                                </CButtonGroup>
                            </td>
                        ),
                    'filepdf':
                        (item) => (
                            <td >
                                <a href={`${WEB_API}${item.file}`} target="_blank">
                                    <img src={pdficon} style={{ height: "1.25rem", width: "1.25rem" }} className='c-login-brand' />
                                </a>
                            </td>
                        ),
                    'createdate':
                        (item) => (
                            <td>
                                {moment(item.date)
                                    // .add(543, "years")
                                    .format("DD-MM-YYYY / HH:mm")}
                            </td>
                        ),
                }}
            />
            <CModal
                show={modalConfirm}
                onClose={setModalConfirm}
                color="danger"
            >
                <CModalHeader closeButton>
                    <CModalTitle>เปลี่ยนสถานะ</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    คุณต้องการที่จะอนุมัติ ?
                </CModalBody>
                <CModalFooter>
                    <CButton onClick={redeemFor} color="primary">ยืนยัน</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalConfirm(false)}
                    >ยกเลิก</CButton>
                </CModalFooter>
            </CModal>

            <CModal
                show={modalConfirm2}
                onClose={setModalConfirm2}
                style={{ width: '133%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>หมายเหตุ</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={unapprove} action="" >
                    <CModalBody >
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="desc">หมายเหตุ<span style={{ color: 'red' }}>*</span></CLabel>
                                            <CTextarea onChange={(e) => { setNote(e.target.value) }} id="description" required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">ยืนยัน</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={() => setModalConfirm2(false)}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >
        </>
    );
};

export default Tables;