import React, { useEffect, useState } from 'react'
import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CModal,
    CModalHeader,
    CModalFooter,
    CButton,
    CLabel,
    CInput
} from '@coreui/react'
import CIcon from "@coreui/icons-react";
import Tables from './Tables';
import ApiTranLogBack from '../../api/ApiTranLogBack';
import ApiMasterRedeem from '../../api/ApiMasterRedeem';
import ApiTranStock from '../../api/ApiTranStock';
import ApiEtaxExpress from '../../api/ApiEtaxExpress';


const MasterRedeem = () => {
    const [dataTable, setDataTable] = useState([]);
    const [rawDataTable, setRawDataTable] = useState([]);
    const [value, setValue] = useState("");
    const [code, setCode] = useState("");
    const [name, setName] = useState("");
    const [balance, setBalance] = useState("");

    useEffect(() => {
        getData();
        return () => {
        }
    }, []);

    const getData = async () => {
        try {
            const result = await ApiEtaxExpress.getAllExExpress();
            if (result.status === 200) {
                const { data } = result.data;
                setRawDataTable(data);
                for (let i = 0; i < data.length; i++) {
                    data[i].order = i+1
                }
                setDataTable(data);
            }
        } catch (error) {

        }
    }

    // const onSearch = async () => {
    //     var model = {
    //         activity_detail: "Menu Config Search",
    //         json: JSON.stringify({
    //             name: name,
    //             stock_balance: balance
    //         })
    //     }
        
    //     if(rawDataTable && rawDataTable.length > 0){
    //         var result = rawDataTable;
    //         if(name && name.length > 0){
    //             result = result.filter((x) => x.name.toLowerCase().includes(name.toLowerCase()));
    //         }
    //         if(balance && balance > 0){
    //             result = result.filter((x) => x.stock_balance.toString().toLowerCase().includes(balance.toLowerCase()));
    //         }
    //         for (let i = 0; i < result.length; i++) {
    //             result[i].order = i+1
    //         }
    //         setDataTable(result);
    //     }
    // }

    // const resetSearchOption = () => {
    //     setName("");
    //     setBalance("");
    //     var result = rawDataTable;
    //     for (let i = 0; i < result.length; i++) {
    //         result[i].order = i+1
    //     }
    //     setDataTable(result);
    // }

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CCard>
                        <CCardHeader>
                            <b> การจัดการส่งใบกำกับภาษี </b>
                        </CCardHeader>
                        <CCardBody>
                            <Tables refreshData={getData} data={dataTable} />
                        </CCardBody>
                    </CCard>
                </CCol>
            </CRow>
        </>
    )
}

export default MasterRedeem