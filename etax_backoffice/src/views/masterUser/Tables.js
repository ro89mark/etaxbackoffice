import React, { useState } from 'react'
import {
    CCardBody,
    CButton,
    CDataTable,
    CModal,
    CModalHeader,
    CModalBody,
    CModalFooter,
    CCol,
    CCard,
    CRow,
    CCardHeader,
    CFormGroup,
    CLabel,
    CInput,
    CSelect,
    CForm,
    CModalTitle,
    CButtonGroup,
    CLink,
    CTextarea
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import ApiMasterConfig from '../../api/ApiMasterConfig';
import Swal from "sweetalert2";
import parse from 'html-react-parser';
import moment from 'moment';
import DatePicker from "react-datepicker";
import ReactQuill from "react-quill";
import "quill/dist/quill.snow.css";
import "react-datepicker/dist/react-datepicker.css";
import ApiMasterRedeem from '../../api/ApiMasterRedeem';
import { CheckFile } from "../../utils/uploadfile";
import { WEB_API } from "../../env";
import ApiMasterCampaign from '../../api/ApiMasterCampaign';
import ApiEtaxMain from '../../api/ApiEtaxMain';
import ApiEtaxUsers from '../../api/ApiEtaxUsers';
const edit_icon = require("./../../assets/icons/edit.svg");
const edit_hover = require("./../../assets/icons/edit_hover.svg");
const trash_icon = require("./../../assets/icons/trash.svg");
const trash_hover = require("./../../assets/icons/trash_hover.svg");

const DemoTable = ({ data = [], refreshData = () => { } }) => {

    const modules = {
        toolbar: [
            ["bold", "italic", "underline", "strike"], // toggled buttons
            ["blockquote", "code-block"],
            [{ header: 1 }, { header: 2 }], // custom button values
            [{ list: "ordered" }, { list: "bullet" }],
            [{ script: "sub" }, { script: "super" }], // superscript/subscript
            [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
            [{ direction: "rtl" }], // text direction
            [{ size: ["small", false, "large", "huge"] }], // custom dropdown
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
            [{ color: [] }, { background: [] }], // dropdown with defaults from theme
            [{ font: [] }],
            [{ align: [] }],
            ["clean"], // remove formatting button
        ],
    };

    const [modalEdit, setModalEdit] = useState(false);
    const [modalEdit2, setModalEdit2] = useState(false);
    const [modalConfirm, setModalConfirm] = useState(false);

    // const [title, setTitle] = useState('')
    // const [description, setDescription] = useState('')
    // const [dataSelected, setDataSelected] = useState('');
    // const [picture, setPicture] = useState('')
    // const [showImage, setShowImage] = useState('')


    const [dataSelected, setDataSelected] = useState('')
    const [firstname, setFirstName] = useState('')
    const [lastname, setLastname] = useState('')
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [role, setRole] = useState('')

    const toggle = () => {
        setModalEdit(!modalEdit);
    }

    const toggleNew = () => {
        setModalEdit2(!modalEdit2);
    }

    const toggleConfirm = () => {
        setModalConfirm(!modalConfirm);
    }

    // const func_attachFile = async (e) => {
    //     if (e.files.length > 0) {
    //         var file = e.files[0];

    //         var type = ["image/png", "image/jpg", "image/jpeg"];
    //         var message = "";
    //         var check = CheckFile({
    //             file,
    //             size: 10,
    //             type: type,
    //             message,
    //         });
    //         if (check) {
    //             setPicture(file)
    //         }
    //     }
    // };

    // const newDetails = () => {
    //     setTitle('')
    //     setDescription('')
    //     setStartDate(new Date())
    //     setEndDate(new Date())
    //     setPicture('')
    //     toggleNew();
    // }

    // const onSubmitNew = (e) => {
    //     e.preventDefault();
    //     var formdata = new FormData();
    //     formdata.append("title", title);
    //     formdata.append("description", description);
    //     formdata.append("start_date", startDate.toISOString());
    //     formdata.append("end_date", endDate.toISOString());
    //     formdata.append("createby", 0);
    //     formdata.append("updateby", 0);
    //     formdata.append("file", picture);
    //     newDataMtCampaign(formdata);
    // }

    const editDetail = (record) => {
        toggleNew();
        console.log(record);
        setFirstName(record.firstname)
        setLastname(record.lastname)
        setUsername(record.username)
        setRole(record.role)
        setDataSelected(record)
    }

    const onSubmitEdit = (e) => {
        e.preventDefault();
        const data = {
            "firstname": firstname,
            "lastname": lastname,
            "username": username,
            "role": role,
            "id" : dataSelected.id
        }
        if (data.role === '' || data.role === 'F') {
            Swal.fire({
                icon: "error",
                title: "กรุณาเลือกตำแหน่ง",
            });
            return
        }
       onEdit(data);
    }
    
    const deleteDetails = (record) => {
        toggleConfirm();
        setDataSelected(record);
    }

    const addUser = (record) => {
        toggle();
        setDataSelected('')
        setFirstName('')
        setLastname('')
        setUsername('')
        setPassword('')
        setRole('')
    }

    const onSubmitRegister = (e) => {

        e.preventDefault();
        const data = {
            "firstname": firstname,
            "lastname": lastname,
            "username": username,
            "password": password,
            "role": role
        }
        if (data.role === '' || data.role === 'F') {
            Swal.fire({
                icon: "error",
                title: "กรุณาเลือกตำแหน่ง",
            });
            return
        }
        registerUser(data)
    }

    const registerUser = async (data) => {
        try {
            const result = await ApiEtaxUsers.registerExUsers(data);
            if (result.status === 200) {
                const { data } = result.data;
                toggle();
                Swal.fire({
                    icon: "success",
                    title: "บันทึกสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            console.log(error.response)
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });

        }
    }


    const onDelete = async () => {
        try {
            const id = dataSelected.id
            const result = await ApiEtaxUsers.deleteUsers(id);
            if (result.status === 200) {
                const { data } = result.data;
                toggleConfirm();
                Swal.fire({
                    icon: "success",
                    title: "ลบสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const onEdit = async (data) => {
        try {
            console.log(data);
            const result = await ApiEtaxUsers.updateUsers(data);
            if (result.status === 200) {
                const { data } = result.data;
                toggleNew();
                Swal.fire({
                    icon: "success",
                    title: "แก้ไขสำเร็จ",
                    timer: 2000,
                }).then((success) => {
                    refreshData();
                });
            }
        } catch (error) {
            Swal.fire({
                icon: "error",
                title: error.response.data,
            });
        }
    }

    const fields = [
        { key: 'order', label: 'ลำดับ', _style: { width: '1%' } },
        {
            key: 'option',
            label: '',
            _style: { width: '1%' },
            filter: false,
        },
        { key: 'firstname', label: "ชื่อ", _style: { width: '16%' } },
        { key: 'lastname', label: "นามสกุล", _style: { width: '16%' } },
        { key: 'username', label: "ชื่อผู้ใช้งาน", _style: { width: '16%' } },
        { key: 'role', label: "ตำแหน่ง", _style: { width: '16%' } },
        { key: 'createdate', label: "วันที่สร้าง", _style: { width: '16%' } },

    ]

    return (
        <>
            <CRow>
                <CCol xs="12" lg="12">
                    <CButton
                        onClick={() => { addUser() }}
                        color="primary-custom"
                        variant="reverse"
                    >
                        <CIcon name="cil-plus" />
                        <span className="ml-2">
                            <b>เพิ่มข้อมูล</b>
                        </span>
                    </CButton>
                </CCol>
            </CRow>
            <CDataTable
                items={data}
                fields={fields}
                tableFilter={{ label: "ค้นหา", placeholder: "พิมพ์คำที่ต้องการค้นหา" }}
                cleaner
                itemsPerPageSelect={{ label: "จำนวนการแสดงผล" }}
                itemsPerPage={10}
                hover
                sorter
                striped
                bordered
                pagination
                scopedSlots={{
                    'order':
                        (item) => (
                            <td style={{ textAlign: 'center' }}>
                                {item.order}
                            </td>
                        ),
                    'option':
                        (item) => (
                            <td className="center">
                                <CButtonGroup>
                                    <CButton
                                        color="primary-custom"
                                        variant="reverse"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { editDetail(item) }}
                                        onMouseOver={(e) => document.getElementById(`edit-${item.order}`).src = edit_hover}
                                        onMouseOut={(e) => document.getElementById(`edit-${item.order}`).src = edit_icon}
                                    >
                                        <img id={`edit-${item.order}`} src={edit_icon} />
                                    </CButton>
                                    <CButton
                                        className="ml-3"
                                        color="danger-custom"
                                        variant="reverse"
                                        shape="square"
                                        size="sm"
                                        onClick={() => { deleteDetails(item) }}
                                        onMouseOver={(e) => document.getElementById(`delete-${item.order}`).src = trash_hover}
                                        onMouseOut={(e) => document.getElementById(`delete-${item.order}`).src = trash_icon}
                                    >
                                        <img id={`delete-${item.order}`} src={trash_icon} />
                                    </CButton>
                                </CButtonGroup>
                            </td>
                        ),
                    'role':
                        (item) => (
                            <td>{item.role === 'A' ? "แอดมิน" : "พนักงาน"}</td>
                        ),
                    'createdate':
                        (item) => (
                            <td>{moment(item.createdate).format('DD-MM-YYYY HH:mm')}</td>
                        ),

                }}
            />

            <CModal
                show={modalConfirm}
                onClose={setModalConfirm}
                color="danger"
            >
                <CModalHeader closeButton>
                    <CModalTitle>ลบรายการ</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    คุณต้องการที่จะลบรายการนี้ ?
                </CModalBody>
                <CModalFooter>
                    <CButton onClick={onDelete} color="primary">ยืนยัน</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalConfirm(false)}
                    >ยกเลิก</CButton>
                </CModalFooter>
            </CModal>

            <CModal
                show={modalEdit}
                onClose={toggle}
                style={{ width: '133%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>เพิ่มข้อมูลผู้ใช้</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmitRegister} action="" >
                    <CModalBody>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> ชื่อ <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={(e) => { setFirstName(e.target.value) }} value={firstname} required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> นามสกุล <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={(e) => { setLastname(e.target.value) }} value={lastname} required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> ชื่อผู้ใช้งาน <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput onChange={(e) => { setUsername(e.target.value) }} value={username} required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> รหัสผ่าน <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput type='password' onChange={(e) => { setPassword(e.target.value) }} value={password} required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                {/* <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="idParent">เลือกสินค้า</CLabel>
                                            <CSelect custom name="idParent" id="idParent" onChange={(e) => setIdRelate(e.target.value)}>
                                                <option>เลือก 1 อย่าง</option>
                                                {mtProduct.map((d, index) =>
                                                    <option key={index} value={d.id}>{d.name}</option>
                                                )}
                                            </CSelect>
                                        </CFormGroup>
                                    </CCol>
                                </CRow> */}
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="idParent">เลือกตำแหน่ง</CLabel>
                                            <CSelect onChange={(e) => setRole(e.target.value)}>
                                                <option value="F">เลือก 1 อย่าง</option>
                                                <option value="A">แอดมิน</option>
                                                <option value="B">พนักงาน</option>
                                            </CSelect>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggle}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >
            <CModal
                show={modalConfirm}
                onClose={setModalConfirm}
                color="danger"
            >
                <CModalHeader closeButton>
                    <CModalTitle>ลบรายการ</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    คุณต้องการที่จะลบรายการนี้ ?
                </CModalBody>
                <CModalFooter>
                    <CButton onClick={onDelete} color="primary">ยืนยัน</CButton>{' '}
                    <CButton
                        color="secondary"
                        onClick={() => setModalConfirm(false)}
                    >ยกเลิก</CButton>
                </CModalFooter>
            </CModal>
            
            <CModal
                show={modalEdit2}
                onClose={toggleNew}
                style={{ width: '133%' }}
            >
                <CModalHeader closeButton>
                    <CModalTitle>แก้ไขข้อมูล</CModalTitle>
                </CModalHeader>
                <CForm onSubmit={onSubmitEdit} >
                    <CModalBody>
                        <CRow>
                            <CCol xs="12" sm="12">
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> ชื่อ <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput id="code" onChange={(e) => { setFirstName(e.target.value) }} value={firstname} required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> นามสกุล <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput id="code" onChange={(e) => { setLastname(e.target.value) }} value={lastname} required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="code"> ชื่อผู้ใช้งาน <span style={{ color: 'red' }}>*</span></CLabel>
                                            <CInput id="code" onChange={(e) => { setUsername(e.target.value) }} value={username} required />
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                                <CRow>
                                    <CCol xs="12">
                                        <CFormGroup>
                                            <CLabel htmlFor="idParent">เลือกตำแหน่ง</CLabel>
                                            <CSelect value={role} onChange={(e) => setRole(e.target.value) }>
                                                <option value="F">เลือก 1 อย่าง</option>
                                                <option value="A">แอดมิน</option>
                                                <option value="B">พนักงาน</option>
                                            </CSelect>
                                        </CFormGroup>
                                    </CCol>
                                </CRow>
                            </CCol>
                        </CRow >
                    </CModalBody>
                    <CModalFooter>
                        <CButton type="submit" color="primary">บันทึก</CButton>{' '}
                        <CButton
                            color="secondary"
                            onClick={toggleNew}
                        >ยกเลิก</CButton>
                    </CModalFooter>
                </CForm >
            </CModal >
        </>
    )
}

export default DemoTable
