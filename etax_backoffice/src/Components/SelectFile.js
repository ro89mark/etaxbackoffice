import React, { useEffect, useState, useRef } from "react";
import { CButton } from "@coreui/react";

const SelectFile = ({ onChange = () => {} }) => {
  const InputFile = useRef(null);

  const func_openSelectfile = () => {
    InputFile.current.click();
  };

  const func_pickFile = () => {
    onChange(InputFile.current);
  };
  return (
    <>
      <CButton
        onClick={() => {
          func_openSelectfile();
        }}
        block
        variant="outline"
        color="info"
      >
        Browse
      </CButton>
      <input
        onChange={(e) => {
          func_pickFile();
        }}
        type="file"
        style={{ display: "none" }}
        ref={InputFile}
      ></input>
    </>
  );
};

export default SelectFile;
