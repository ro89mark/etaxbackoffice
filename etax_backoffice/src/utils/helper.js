const arraytostringcomma = async (arr = []) => {
  var str = "";
  console.log(arr);
  try {
    for (let i = 0; i < arr.length; i++) {
      if (i == arr.length - 1) {
        str += `${arr[i]}`;
      } else {
        str += `${arr[i]},`;
      }
    }
  } catch {
    str = "";
  }
  return str;
};

const stringcommatoarray = async (str = "") => {
  var arr = [];
  try {
    var strsplit = str.split(",");
    for (let i = 0; i < strsplit.length; i++) {
      arr.push(strsplit[i]);
    }
  } catch {
    arr = [];
  }
  return arr;
};

const helper = {
  arraytostringcomma: arraytostringcomma,
  stringcommatoarray: stringcommatoarray,
};

export default helper;
