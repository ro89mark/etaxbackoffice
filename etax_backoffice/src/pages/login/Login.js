import React, { useRef, useState } from 'react';
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
} from '@coreui/react';
import { Link } from 'react-router-dom';
import CIcon from '@coreui/icons-react';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import ApiUser from '../../api/ApiUser.js';
import ApiEtaxUsers from '../../api/ApiEtaxUsers';
import { setLocalStorage } from '../../utils/localStorage.js';
import { useSelector, useDispatch } from 'react-redux';

const logo = require('../../images/login_logo.png');

const Login = () => {

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const login = async (e) => {
    e.preventDefault();

    const data = {
      username: username,
      password: password
    }

    try {
      const result = await ApiEtaxUsers.loginExUsers(data);
      if (result.status === 200) {
        const { token } = result.data;
        console.log(token)
        localStorage.setItem('token', token);
        Swal.fire({
          icon: "success",
          title: "เข้าสู่ระบบสำเร็จ",
          timer: 2000,
        }).then((success) => {
          window.location = '/'
        });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "เข้าสู่ระบบไม่สำเร็จ",
      });
    }

  }


  return (
    <div className='c-app c-default-layout bg-light flex-row align-items-center'>
      <CContainer>
        <CRow className='justify-content-center'>
          <CCol md='6'>
            <CCardGroup>
              <CCard className='p-4 border'>
                <CCardBody>
                  <form onSubmit={login}>
                    <h1>Login</h1>
                    <p className='text-muted'>Sign In to your account</p>
                    <CInputGroup className='mb-3'>
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name='cil-user' />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        required
                        type='text'
                        name='username'
                        placeholder='Username'
                        autoComplete={'off'}
                        onChange={(e) => setUsername(e.target.value)}
                      />
                    </CInputGroup>
                    <CInputGroup className='mb-4'>
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name='cil-lock-locked' />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        required
                        type='password'
                        name='password'
                        placeholder='Password'
                        autoComplete={'off'}
                        onChange={(e) => setPassword(e.target.value)}
                      />
                    </CInputGroup>
                    <CRow
                      style={{
                        display: 'flex',
                        justifyContent: 'center',
                      }}
                    >
                      <div>
                        <CButton
                          className='px-3'
                          // onClick={login}
                          type='submit'
                          color='warning-custom'
                        >
                          Login
                        </CButton>
                        {/* <Link to="/home">
                          <CButton color="primary" className="px-4">
                            Login
                          </CButton>
                        </Link> */}
                      </div>
                    </CRow>
                  </form>
                </CCardBody>
              </CCard>
              <CCard
                className='text-black bg-blue py-5 d-md-down-none'
                style={{ width: '34%' }}
              >
                {/* <CCardBody className='text-center d-flex-center'>
                  <img src={logo} className='c-login-brand' />
                  <br />
                </CCardBody> */}
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
