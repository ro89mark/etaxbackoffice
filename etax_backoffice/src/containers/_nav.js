import React from "react";
const file_spreadsheet_duotone = require("./../assets/icons/file-spreadsheet-duotone.svg");
const user_duo = require("./../assets/icons/user-duotone-5.svg");

const role = localStorage.getItem('role')

export default [
  {
    _tag: "CSidebarNavTitle",
    _children: ["จัดการข้อมูลในระบบ"],
    id: "settingLabel",
  },

  {
    _tag: "CSidebarNavItem",
    name: "หน้าหลัก",
    code: "main",
    to: "/main",
    icon: "cil-settings",
  },
  {
    _tag: "CSidebarNavItem",
    name: "การจัดการคืนเงิน",
    code: "masterrefund",
    to: "/MasterRefund",
    icon: "cil-settings",
  },
  {
    _tag: "CSidebarNavItem",
    name: "การจัดการออกใบกำกับภาษี",
    code: "masteretax",
    to: "/masteretax",
    icon: "cil-settings",
  },
  {
    _tag: "CSidebarNavItem",
    name: "การจัดการส่งใบกำกับภาษี",
    code: "masterexpress",
    to: "/masterexpress",
    icon: "cil-settings",
  },
  {
    _tag: "CSidebarNavItem",
    name: "ประวัติการจัดส่งใบกำกับภาษี",
    code: "tranhistory",
    to: "/tranhistory",
    icon: "cil-settings",
  },
  {
    _tag: "CSidebarNavItem",
    name: "การจัดการผู้ใช้งาน",
    code: "masterUser",
    to: "/masterUser",
    icon: "cil-settings",
  },
];
