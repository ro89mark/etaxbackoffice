import React, { Suspense, useEffect } from 'react'
import {
  Redirect,
  Route,
  Switch
} from 'react-router-dom'
import { CContainer, CFade } from '@coreui/react'
import ApiEtaxUsers from '../api/ApiEtaxUsers';
import Swal from 'sweetalert2';

// routes config
import routes from '../routes'

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

const TheContent = () => {

  // useEffect(() => {
  //   const token = localStorage.getItem('token')
  //   authenToken(token)
  // });

  // const authenToken = async (token) => {
  //   try {
  //     const result = await ApiEtaxUsers.authenExUsers(token);
  //     if (result.status === 200) {
  //       const { data } = result.data
  //       localStorage.setItem('username', data.username);
  //       localStorage.setItem('role', data.role);
  //       console.log(data.username)
  //       console.log(data.role)
  //     }
  //   } catch (error) {
  //     Swal.fire({
  //       icon: "error",
  //       title: "เข้าสู่ระบบไม่สำเร็จ",
  //     });
  //     window.location = '#/login'
  //   }
  // }

  return (
    <main className="c-main">
      <CContainer fluid>
        <Suspense fallback={loading}>
          <Switch>
            {routes.map((route, idx) => {
              return route.component && (
                <Route
                  key={idx}
                  path={route.path}
                  exact={route.exact}
                  name={route.name}
                  render={props => (
                    <CFade>
                      <route.component {...props} />
                    </CFade>
                  )} />
              )
            })}
            <Redirect from="/" to="/main" />
          </Switch>
        </Suspense>
      </CContainer>
    </main>
  )
}

export default React.memo(TheContent)
