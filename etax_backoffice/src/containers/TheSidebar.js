import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

// sidebar nav config
import navigation from "./_nav";

const logoAsl = require('../images/AslLogo.png')
const TheSidebar = () => {
  const dispatch = useDispatch();
  const show = useSelector((state) => state.sidebarShow);
  const [nevItem, setNevItem] = useState([]);
  const userState = useSelector((state) => state.user);
  const menuState = useSelector((state) => state.menu);

  useEffect(() => {
    handlePermission();
    return () => {};
  }, [userState, menuState]);

  useEffect(() => {}, []);

  const handlePermission = () => {
    if(userState.role && menuState){
      // console.log("menuState", menuState);
      // const newNav = [];
      // for (let i = 0; i < navigation.length; i++) {
      //   // console.log("navigation[i]::", navigation[i]);
      //   if(navigation[i]._tag == "CSidebarNavTitle"){
      //     newNav.push(navigation[i]);
      //   }else{        
      //     if(navigation[i].code == "menu" || navigation[i].code == "group_permission" || navigation[i].code == "setting" || navigation[i].code == "logtransaction" || navigation[i].code == "logfontend" || navigation[i].code == "logbackend"){
      //       if (userState.role.role_code == "admin"){
      //         newNav.push(navigation[i]);
      //       }
      //     }else if (navigation[i].code == "verify_card"){
      //       if(userState.team_pm_verify_card == 1){
      //         newNav.push(navigation[i]);
      //       }
      //     }else{
      //       var permission = menuState.find((x) => x.menu_code == navigation[i].code);
      //       // console.log("navigation[i].code", navigation[i].code);
      //       // console.log("permission", permission);
      //       // console.log("userState.role.role_code", userState.role.role_code);
      //       if(permission){
      //         if(userState.role.role_code == "admin" && permission.active_admin == 1){
      //           newNav.push(navigation[i]);
      //         }else if(userState.role.role_code == "user_branch" && permission.active_user_branch == 1){
      //           newNav.push(navigation[i]);
      //         }else if(userState.role.role_code == "user_team" && permission.active_user_team == 1){
      //           newNav.push(navigation[i]);
      //         }else if(userState.role.role_code == "user" && permission.active_user == 1){
      //           newNav.push(navigation[i]);
      //         }
      //       }
      //     }
      //   }
      // }
      // setNevItem(newNav);
      // setNevItem(navigation);
    }
  }

  return (
    <CSidebar
      colorScheme="custom"
      show={show}
      foldable
      onShowChange={(val) => dispatch({ type: "set", sidebarShow: val })}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        {/* <div>
          <img width="40" src={logoAsl}/>
        </div> */}
        <div>Back Office</div>
      </CSidebarBrand>
      <CSidebarNav>
        <CCreateElement
          items={navigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle,
          }}
        />
      </CSidebarNav>
    </CSidebar>
  );
};

export default React.memo(TheSidebar);
