import React from 'react'
import {VERSION_RELEASE} from '../env'
//import { CFooter, CLink } from '@coreui/react'

const TheFooter = () => {
  return (
    <div>{VERSION_RELEASE}</div>
  )
}

export default React.memo(TheFooter)
