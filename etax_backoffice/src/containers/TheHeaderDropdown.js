import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,

} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useHistory, useLocation } from "react-router-dom";
import Swal from "sweetalert2";
import { setLocalStorage } from "../utils/localStorage.js";
import ApiEtaxUsers from '../api/ApiEtaxUsers';

const TheHeaderDropdown = (props) => {
  const dispatch = useDispatch();
  const userState = useSelector((state) => state.user);
  const history = useHistory();
  const [username, setUsername] = useState('');

  useEffect(() => {
    const token = localStorage.getItem('token')
    authenToken(token)
  });

  const authenToken = async (token) => {
    try {
      const result = await ApiEtaxUsers.authenExUsers(token);
      if (result.status === 200) {
        const { data } = result.data
        localStorage.setItem('username', data.username);
        localStorage.setItem('role', data.role);
        setUsername(data.username)
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "เข้าสู่ระบบไม่สำเร็จ",
      });
      window.location = '#/login'
    }
  }

  const changeRole = (item) => {
    Swal.fire({
      title: "",
      text: `Change Role To ${item.role_name}`,
      icon: "question",
      showCancelButton: true,
      cancelButtonText: "Cancel",
      confirmButtonText: "Confirm",
    }).then((result) => {
      if (result.isConfirmed) {
        const user = { ...userState };
        user.role = item;
        setLocalStorage("role", item.role_name);
        dispatch({ type: "set_user", user: user });
        setTimeout(() => {
          history.push("/");
          history.go(0);
        }, 500);
      }
    });
  };

  const logout = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    localStorage.removeItem('role');
    window.location = window.location = '#/login'
  };

  return (
    <>
      <CDropdown inNav className="c-header-nav-items mx-2" direction="down">
        <CDropdownToggle className="c-header-nav-link" caret={false}>
          <span className="mr-2">{username}</span>
        </CDropdownToggle>
        <CDropdownMenu className="pt-0" placement="bottom-end">
          <CDropdownItem header tag="div" color="light" className="text-center">
            <strong>ชื่อผู้ใช้งาน : {userState.username}</strong>
          </CDropdownItem>
          {userState.roles ? userState.roles.map((item, index) => (
            <CDropdownItem
              onClick={() =>
                item.role_code == userState.role.role_code
                  ? null
                  : changeRole(item)
              }
            >
              {item.role_code == userState.role.role_code && (
                <CIcon
                  style={{ color: "#8CC63F" }}
                  color="success"
                  name="cil-check-alt"
                  className="mfe-2"
                />
              )}
              {item.role_name}
            </CDropdownItem>
          )) : ""}
          <CDropdownItem divider />
          <CDropdownItem onClick={() => logout()}>
            <CIcon name="cil-lock-locked" className="mfe-2" />
            ออกจากระบบ
          </CDropdownItem>
        </CDropdownMenu>
      </CDropdown>
    </>
  );
};

export default TheHeaderDropdown;
