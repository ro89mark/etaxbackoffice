import React from "react";

// const User = React.lazy(() => import("./views/user"));
// const Mt_tag = React.lazy(() => import("./views/mt_tag"));
// const Mt_category = React.lazy(() => import("./views/mt_category"));
// const Mt_banner = React.lazy(() => import("./views/masterBanner"));
// const UserDetail = React.lazy(() => import("./views/user/detail"));
// const EmailTemplate = React.lazy(() => import("./views/emailTemplate"));
// const EmailTemplateDetail = React.lazy(() =>
//   import("./views/emailTemplate/detail")
// );
// const LogTransactionFont = React.lazy(() =>
//   import("./views/logTransactionFont/LogTransactionFont")
// );
// const LogTransactionBack = React.lazy(() =>
//   import("./views/logTransactionBack/LogTransactionBack")
// );
// const TranRedeem = React.lazy(() => import("./views/tranRedeem"));
// const MasterRedeem = React.lazy(() => import("./views/masterRedeem"));
// const MasterPromotion = React.lazy(() => import("./views/masterPromotion"));
// const Mt_product = React.lazy(() => import("./views/mt_product"));
// const MasterCampaign = React.lazy(() => import("./views/masterCampaign"));

const MasterMain = React.lazy(() => import("./views/masterMain"));
const MasterEtax = React.lazy(() => import("./views/masterEtax"));
const MasterExpress = React.lazy(() => import("./views/masterExpress"));
const TranHistory = React.lazy(() => import("./views/tranHistory"));
const MasterUser = React.lazy(() => import("./views/masterUser"))
const MasterRefund = React.lazy(() => import("./views/masterRefund"))


// const TranStock = React.lazy(() => import("./views/tranStock"));

// const Mt_productEdit = React.lazy(() =>
//   import("./views/mt_product/ProductEdit")
// );
// const LogTransactionActivity = React.lazy(() => import('./views/logTransactionActivity/LogTransactionActivity'))
// const SearchVerify = React.lazy(() => import('./views/searchVerify/SearchVerify'))
// const Branch = React.lazy(() => import('./views/branch'))
// const Team = React.lazy(() => import('./views/team'))
// const UserTeam = React.lazy(() => import('./views/userTeam'))
// const UserRole = React.lazy(() => import('./views/userRole'))
// const Menu = React.lazy(() => import('./views/menuPermission'))
// const Config = React.lazy(() => import("./views/masterConfig"));
// const GroupPsPermission = React.lazy(() => import('./views/groupPsPermission'))
// const VerifyCard = React.lazy(() => import('./views/verifyCard'))
// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  // { path: "/", exact: true, name: "หน้าหลัก" },
  // { path: "/user", name: "ผู้ใช้งาน", component: User },
  // { path: "/userdetail", name: "ผู้ใช้งาน", component: UserDetail },
  // { path: "/emailtemplate", name: "Email Template", component: EmailTemplate },
  // {
  //   path: "/emailtemplatedetail",
  //   name: "Email Template",
  //   component: EmailTemplateDetail,
  // },
  // {
  //   path: "/logtransaction/fontend",
  //   name: "Activity Log (Front End)",
  //   component: LogTransactionFont,
  // },
  // {
  //   path: "/logtransaction/backend",
  //   name: "Activity Log (Back End)",
  //   component: LogTransactionBack,
  // },
  // { path: "/tranredeem", name: "Tran Redeem", component: TranRedeem },
  // { path: "/mtredeem", name: "Master Redeem", component: MasterRedeem },
  // { path: "/mtpromotion", name: "Master Promotion", component: MasterPromotion, },
  // { path: "/transtock", name: "Tran Stock", component: TranStock },
  // { path: "/mtcampaign", name: "Master Campaign", component: MasterCampaign },
  { path: "/", exact: true, name: "หน้าหลัก" },
  { path: "/main", name: "Main", component: MasterMain },
  { path: "/masteretax", name: "Etax", component: MasterEtax },
  { path: "/masterexpress", name: "Express", component: MasterExpress },
  { path: "/tranhistory", name: "Tran History", component: TranHistory },
  { path: "/masterUser", name: "User", component: MasterUser },
  { path: "/MasterRefund", name: "Master Refund", component: MasterRefund },

  // { path: '/logtransaction/activitylog', name: 'Activity Log', component: LogTransactionActivity },
  // { path: '/searchverify', name: 'ค้นหารายการยืนยันตัวตน', component: SearchVerify },
  // { path: "/setting", name: "ตั้งค่าระบบ", component: Config },
  // { path: '/branch', name: 'ฐานข้อมูลสาขา / ทีม', component: Branch },
  // { path: '/team', name: 'ฐานข้อมูล ทีม', component: Team },
  // { path: '/user_team', name: 'ฐานข้อมูล ผู้ร่วมทีม', component: UserTeam },
  // { path: '/user_role', name: 'สิทธิการเข้าถึง ผู้ใช้งาน', component: UserRole },
  // { path: '/menu', name: 'สิทธิ์การเข้าถึงเมนู', component: Menu },
  // { path: '/group_permission', name: 'ตั้งค่ากลุ่ม/ สิทธิ์ข้อมูลตัวตน', component: GroupPsPermission },
  // { path: '/verify_card', name: 'ตรวจสอบสถานะบัตรประชาชน', component: VerifyCard },
  // { path: "/MtTag", name: "Master_tag", component: Mt_tag },
  // { path: "/MtCategory", name: "Master_category", component: Mt_category },
  // { path: "/Mtbanner", name: "Master_banner", component: Mt_banner },
  // { path: "/Mt_product", name: "Master_product", component: Mt_product },
  // {
  //   path: "/Mt_productEdit",
  //   name: "Master_productEdit",
  //   component: Mt_productEdit,
  // },
];
export default routes;
