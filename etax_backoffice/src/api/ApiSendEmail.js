import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiSendEmail extends Component {

    static sendEmail = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `sendmail`,
            method: "post",
            data : data
        });
        return result;
    };
    
}

export default ApiSendEmail;