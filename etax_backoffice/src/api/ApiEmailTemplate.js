import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

export default class ApiUser extends Component {
  
  static get = async () => {
      await setHeaderAuth();
      const result = await axios({
          url: `mt_email_template`,
          method: "get",
      });
      return result;
  };
  static getDetail = async (id) => {
      await setHeaderAuth();
      const result = await axios({
          url: `mt_email_template/${id}`,
          method: "get",
      });
      return result;
  };
  static save = async (data) => {
      await setHeaderAuth();
      const result = await axios({
          url: "mt_email_template",
          method: data.id > 0 ? "put" : "post",
          data: data
      });
      return result;
  }
}
