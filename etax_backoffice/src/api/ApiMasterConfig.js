import axios from 'axios';
import { Component } from 'react';
import { setHeaderAuth } from "../utils";

class ApiMasterConfig extends Component {
    static get = async (country) => {
        await setHeaderAuth();
        const result = await axios({
            url: `mt_config`,
            method: "get",
        });
        return result;
    };
    static save = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: "mt_config",
            method: data.id > 0 ? "put" : "post",
            data: data
        });
        return result;
    }
    static delete = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `mt_config/${data.id}`,
            method: "delete",
        });
        return result;
    }
}


export default ApiMasterConfig;