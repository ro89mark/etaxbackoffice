import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiEtaxRefund extends Component {

    static getAllExRefund = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `getInvoiceRefund`,
            method: "get",
        });
        return result;
    };

    static updateExRefund = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: data.paystatus == "O" ? "updateOverRefund" : "updateUnderRefund",
            method: "post",
            data: data
        });
        return result;
    };
    
}

export default ApiEtaxRefund;