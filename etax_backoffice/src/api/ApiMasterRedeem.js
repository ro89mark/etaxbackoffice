import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiMasterRedeem extends Component {

    static getAllMtRedeem = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `mtredeem`,
            method: "get",
        });
        return result;
    };
    static updateMtRedeem = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            headers: {
                "Content-Type": "multipart/form-data",
            },
            url: `mtredeem`,
            method: "put",
            data: data
        });
        return result;
    };
    static deleteMtRedeem = async (id) => {
        await setHeaderAuth();
        const result = await axios({
            url: `mtredeem/${id}`,
            method: "delete",
        });
        return result;
    };

    static insertDataMtRedeem = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            headers: {
                "Content-Type": "multipart/form-data",
            },
            url: `mtredeem`,
            method: "post",
            data: data
        });
        return result;
    };
}

export default ApiMasterRedeem;