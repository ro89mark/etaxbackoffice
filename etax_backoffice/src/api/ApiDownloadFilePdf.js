import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiEtaxExpress extends Component {

    static downloadFilePdf = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `downloadfilepdf`,
            method: "post",
            data : data
        });
        return result;
    };
    
}

export default ApiEtaxExpress;