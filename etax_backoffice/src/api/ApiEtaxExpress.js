import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiEtaxExpress extends Component {

    static getAllExExpress = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `getInvoiceExpress`,
            method: "get",
        });
        return result;
    };
    
}

export default ApiEtaxExpress;