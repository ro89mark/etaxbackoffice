import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

export default class ApiMasterValue extends Component {
  static getddlsize = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_value/getddlsize`,
      method: "get",
    });
    return result;
  };
}
