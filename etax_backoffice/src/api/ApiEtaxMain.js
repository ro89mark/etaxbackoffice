import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiEtaxMain extends Component {

    static getAllMtCampaign = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `mtcampaign`,
            method: "get",
        });
        return result;
    };

    static getAllExMain = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `getInvoiceMain`,
            method: "get",
        });
        return result;
    };

    static updateExMain = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `updateInvoiceMain`,
            method: "put",
            data: data
        });
        return result;
    };

    static deleteExMain = async (id) => {
        await setHeaderAuth();
        const result = await axios({
            url: `deleteInvoiceMain/${id}`,
            method: "delete",
        });
        return result;
    };
    
}

export default ApiEtaxMain;