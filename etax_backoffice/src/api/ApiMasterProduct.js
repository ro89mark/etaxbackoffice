import axios from "axios";
import { Component } from "react";
import { setHeaderAuth, setHeaderFromDataAuth } from "../utils";

export default class ApiMasterProduct extends Component {
  static get = async () => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_product/getlist`,
      method: "get",
    });
    return result;
  };

  static getedit = async (id) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_product/getedit?pid=${id}`,
      method: "get",
    });
    return result;
  };

  static getlistidcommanotequeal = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_product/getlistidcommanotequeal`,
      method: "post",
      data: data,
    });
    return result;
  };

  static getlistbyidcomma = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: `mt_product/getlistbyidcomma`,
      method: "post",
      data: data,
    });
    return result;
  };

  static delete = async (data) => {
    await setHeaderAuth();
    const result = await axios({
      url: "mt_product",
      method: "delete",
      data: data,
    });
    return result;
  };

  static save = async (data, id) => {
    await setHeaderAuth();
    const result = await axios({
      headers: {
        "Content-Type": "multipart/form-data",
      },
      url: "mt_product",
      method: id > 0 ? "put" : "post",
      data: data,
    });
    return result;
  };
  static uploadFile = async (data) => {
    const result = await axios({
      headers: {
        "Content-Type": "multipart/form-data",
      },
      url: "mt_product/uploadfile",
      method: "post",
      data: data,
    });
    return result;
  };
}
