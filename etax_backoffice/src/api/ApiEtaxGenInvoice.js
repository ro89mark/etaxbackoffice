import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiEtaxGenInvoice extends Component {

    static getAllExGenInvoice = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `getInvoiceForGen`,
            method: "get",
        });
        return result;
    };

    static genPdf = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `generateInvoice`,
            method: "post",
            data: data
        });
        return result;
    };
    
}

export default ApiEtaxGenInvoice;