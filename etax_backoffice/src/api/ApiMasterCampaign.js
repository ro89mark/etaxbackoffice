import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiMasterCampaign extends Component {

    static getAllMtCampaign = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `mtcampaign`,
            method: "get",
        });
        return result;
    };
    static updateMtCampaign = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            headers: {
                "Content-Type": "multipart/form-data",
            },
            url: `mtcampaign`,
            method: "put",
            data: data
        });
        return result;
    };
    static deleteMtCampaign = async (id) => {
        await setHeaderAuth();
        const result = await axios({
            url: `mtcampaign/${id}`,
            method: "delete",
        });
        return result;
    };

    static insertDataMtCampaign = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            headers: {
                "Content-Type": "multipart/form-data",
            },
            url: `mtcampaign`,
            method: "post",
            data: data
        });
        return result;
    };
}

export default ApiMasterCampaign;