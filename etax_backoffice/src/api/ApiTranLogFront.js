import axios from 'axios';
import { Component } from 'react';
import { setHeaderAuth } from "../utils";

class ApiTranLogFront extends Component {
    static get = async (country) => {
        await setHeaderAuth();
        const result = await axios({
            url: `tran_log_front`,
            method: "get",
        });
        return result;
    };
}


export default ApiTranLogFront;