import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiEtaxHistory extends Component {

    static getAllExHistory = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `getExpressHistory`,
            method: "get",
        });
        return result;
    };
    
}

export default ApiEtaxHistory;