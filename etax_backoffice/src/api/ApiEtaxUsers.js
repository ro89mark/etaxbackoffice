import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiEtaxUsers extends Component {

    static getAllExUsers = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `getMasterUsers`,
            method: "get",
        });
        return result;
    };

    static registerExUsers = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `register`,
            method: "post",
            data: data
        });
        return result;
    };

    static loginExUsers = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `login`,
            method: "post",
            data: data
        });
        return result;
    };
    
    static authenExUsers = async (token) => {
        const result = await axios({
            url: `authen`,
            method: "post",
            headers: {
                Authorization: `Bearer ${token}`,
            }
        });
        return result;
    };

    static deleteUsers= async (id) => {
        await setHeaderAuth();
        const result = await axios({
            url: `deleteUsers/${id}`,
            method: "delete",
        });
        return result;
    };

    static updateUsers = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `updateUsers`,
            method: "put",
            data: data
        });
        return result;
    };
    
}

export default ApiEtaxUsers;