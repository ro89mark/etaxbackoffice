import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiMasterRedeem extends Component {

    static getAllMtPromotion = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `mtpromotion`,
            method: "get",
        });
        return result;
    };

    static updateMtPromotion = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `mtpromotion`,
            method: "put",
            data: data
        });
        return result;
    };

    static deleteMtPromotion = async (id) => {
        await setHeaderAuth();
        const result = await axios({
            url: `mtpromotion/${id}`,
            method: "delete",
        });
        return result;
    };

    static insertDataPromotion = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `mtpromotion`,
            method: "post",
            data : data
        });
        return result;
    };
}


export default ApiMasterRedeem;