import axios from "axios";
import { Component } from "react";
import { setHeaderAuth } from "../utils";

class ApiTranRedeem extends Component {
    static getAllTranRedeem = async () => {
        await setHeaderAuth();
        const result = await axios({
            url: `tranredeem`,
            method: "get",
        });
        return result;
    };
    static updateRedeemStatus = async (id,update) => {
        await setHeaderAuth();
        const result = await axios({
            url: `tranredeem/${id}/${update}`,
            method: "put",
        });
        return result;
    };
    static unapproveRedeemStatus = async (data) => {
        await setHeaderAuth();
        const result = await axios({
            url: `tranredeem/unapprove`,
            method: "put",
            data: data
        });
        return result;
    };
 
}


export default ApiTranRedeem;