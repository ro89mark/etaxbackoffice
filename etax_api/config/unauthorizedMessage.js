const unauthorizedMessage = { 
    error: 'Unauthorized', 
    message: 'Authentication failed' 
}

const existingUserMessage = { 
    title: 'Bad request', 
    message: 'This username has been already' 
}

module.exports = { unauthorizedMessage, existingUserMessage }