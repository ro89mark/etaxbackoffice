const { Pool, Client } = require('pg');

// const pool = new Pool({
//   user: 'postgres',
//   host: '159.65.3.230',
//   database: 'brudb',
//   password: 'P@ssw0rdApp',
//   port: 5432,
// });

// const client = new Client({
//   user: 'postgres',
//   host: '159.65.3.230',
//   database: 'brudb',
//   password: 'P@ssw0rdApp',
//   port: 5432,
// });

const pool = new Pool({
  user: 'postgres',
  host: 'etaxexpress.clfyaped8ft1.ap-southeast-1.rds.amazonaws.com',
  database: 'etaxexpress',
  password: 'MarkDezz007',
  port: 5432,
});

const client = new Client({
  user: 'postgres',
  host: 'etaxexpress.clfyaped8ft1.ap-southeast-1.rds.amazonaws.com',
  database: 'etaxexpress',
  password: 'MarkDezz007',
  port: 5432,
});

// const pool = new Pool({
//   user: 'postgres',
//   host: '10.50.250.95',
//   database: 'asl_verify_db',
//   password: 'dopa#112233',
//   port: 5432,
// });

// const client = new Client({
//   user: 'postgres',
//   host: '10.50.250.95',
//   database: 'asl_verify_db',
//   password: 'dopa#112233',
//   port: 5432,
// });

module.exports = {
  async query(text, params) {
    const start = Date.now();
    const res = await pool.query(text, params);
    const duration = Date.now() - start;
    return res;
  },
  async getClient() {
    const client = await pool.connect();
    const query = client.query;
    const release = client.release;
    // set a timeout of 5 seconds, after which we will log this client's last query
    const timeout = setTimeout(() => {
      console.error('A client has been checked out for more than 5 seconds!');
      console.error(
        `The last executed query on this client was: ${client.lastQuery}`
      );
    }, 5000);
    // monkey patch the query method to keep track of the last query executed
    client.query = (...args) => {
      client.lastQuery = args;
      return query.apply(client, args);
    };
    client.release = () => {
      // clear our timeout
      clearTimeout(timeout);
      // set the methods back to their old un-monkey-patched version
      client.query = query;
      client.release = release;
      return release.apply(client);
    };
    return client;
  },

  async clientQuery(text, params) {
    client.connect();
    const res = await pool.query(text, params);
    client.end();
    return res;
  },
};
