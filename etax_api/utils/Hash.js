const crypto = require('crypto');

exports.HashPassword = async (password, algorithm) =>{
    return crypto.createHash(algorithm).update(password).digest('base64');
}