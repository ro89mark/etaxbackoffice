const models = require('../models/mt_user');
const configs = require('../models/mt_config');
const moment = require('moment');
const hash = require('../utils/Hash')

exports.validate_change_password = async (user, password) => {
    var minlen = await configs.findValueByCode({code: 'pass_min_len'});
    var history_len = await configs.findValueByCode({code: 'pass_history_len'});
    var complexity = await configs.findValueByCode({code: 'pass_complexity'});
    var min_age = await configs.findValueByCode({code: 'pass_min_age'});
    // if(user.last_password_date){
    //     if(parseInt(min_age) > 0){
    //         if(moment(user.last_password_date).add(parseInt(min_age), 'hours') > moment()){
    //             return({success: false, message: `ยังไม่สามารถเปลี่ยนรหัสผ่านได้ภายใน ${moment(user.last_password_date).add(parseInt(min_age), 'hours').format("HH:mm/DD-MM-YYYY")}`});
    //             // throw(`ยังไม่สามารถเปลี่ยนรหัสผ่านได้ภายใน ${moment(user.last_password_date).add(parseInt(min_age)).format("HH:mm/DD-MM-YYYY")}`)
    //         }
    //     }
    // }
    if(password.length < minlen){
        return({success: false, message: `รหัสผ่านไม่ถึงความยาวที่กำหนด (${minlen} ตัวอักษร)`});
    }
    var pass_his = await models.getPassHistory({
        user_id: user.id,
        length: history_len
    })
    var is_dup = false;
    for (let i = 0; i < pass_his.length; i++) {
        if(pass_his[i].password == await hash.HashPassword(password, "sha256")){
            is_dup = true;
        }
    }
    if(is_dup){
        return({success: false, message: `รหัสผ่านซ้ำกับรหัสผ่านครั้งก่อน (ภายใน ${history_len} ครั้ง)`});
    }

    var complexity_lv = 0;
    var SpecialChars = "(!@#$%^&+)(*)_\:}{?>)";
    var passChar = password.split();
    if(passChar.some((x) => isNaN(x))){
        complexity_lv += 1;
    }
    if(passChar.some((x) => (/[a-z]/.test(x)))){
        complexity_lv += 1;
    }
    if(passChar.some((x) => (/[A-Z]/.test(x)))){
        complexity_lv += 1;
    }
    if(passChar.some((x) => SpecialChars.includes(x))){
        complexity_lv += 1;
    }

    if (complexity_lv < complexity){
        return({success: false, message: `รหัสผ่านต้องมีความซับซ้อนมากกว่านี้`});
    }

    return({success: true});
}