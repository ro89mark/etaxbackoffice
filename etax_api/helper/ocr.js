const fs = require("fs");
const request = require('request');

const Ocr = async (file) => {

    const ocr = await request({
        method: "POST",
        uri: "https://api.aiforthai.in.th/ocr",
        headers: {
            "Apikey": "LgArg8PNY2BiY1cmtFsE1XXN6bP6O903",
            "Content-Type": "multipart/form-data"
        },
        formData: {
            "uploadfile": fs.createReadStream(file)
        },
        json: true
    })
    return ocr

};

module.exports = {
    Ocr
};