const express = require("express");
const cors = require("cors");
const config = require("./config/app.js");
const compression = require("compression");
const middlewareErrorParser = require("./middleware/ErrorParser");
const middlewarePathLogger = require("./middleware/PathLogger");
const routes = require("./routes");
var path = require("path");
const app = express();

app.use(express.json({ limit: "25mb" }));
app.use(express.urlencoded({ limit: "25mb" }));
app.use(express.json({ type: "application/json" }));
app.use(express.urlencoded({ extended: false }));
app.use('*/docs',express.static('docs'));
app.use(express.static(path.join(__dirname, "public")));
app.use("/api/", express.static(path.join(__dirname, "public")));
// app.use('/protected/*', file_protected);

// add cors headers
app.use(cors());
// comporess output
app.use(compression());

// only on debug mode
if (config.debug) {
  // path logger
  app.use(middlewarePathLogger);
}

function userIsAllowed(callback) {
  // this function would contain your logic, presumably asynchronous,
  // about whether or not the user is allowed to see files in the
  // protected directory; here, we'll use a default value of "false"
  callback(false);
}

// use routes

app.use("/api/", routes);
app.use(
  "/api/private/",
  express.static(path.join(__dirname, "private"))
);

app.use(middlewareErrorParser);

// Start server
app.listen(process.env.PORT, () => {
  console.log(
    "Express server listening on %d, in %s mode",
    // config.port,
    process.env.PORT,
    app.get("env")
  );
});

app.get("/api/", (req, res) => {
  res.send("BRU BACKOFFICE API START \n");
});

module.exports = app;

// const express = require("express");
// const app = express();

// const cors = require("cors");

// require("./config/config.js")
// const router = require("./router/index.js");

// const port = 8888;

// app.use(express.json({ limit: "25mb" }));
// app.use(express.urlencoded({ limit: "25mb" }));
// app.use(express.json({ type: "application/json" }));
// app.use(express.urlencoded({ extended: false }));

// app.use(cors());

// app.listen(port, () => {
//   console.log(
//     `Express server listening on ${port} Link = http://localhost:8888/api`
//   );
// });

// app.use("/api/", router);
// app.get("/api/", (req, res) => {
//   res.status(200).send("ASL-VERIFY API");
// });

// module.exports = app;
