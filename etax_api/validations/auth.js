const { Joi } = require('express-validation');

const loginValidation = {
    body: Joi.object({
        username: Joi.string().required(),
        password: Joi.string().required()
    })
}

// const createUserParam = {
//     body: {
//         username: Joi.string().min(6).max(15).required(),
//         password: Joi.string().required(),
//         name: Joi.string().required(),
//         email: Joi.string().email().required(),
//         permissions: Joi.array().items(Joi.number())
//     }
// }

// const updateUserParam = {
//     params: {
//         id: Joi.string().required()
//     },
//     body: {
//         name: Joi.string().required(),
//         email: Joi.string().required(),
//         status: Joi.number().required(),
//         permissions: Joi.array().items(Joi.number())
//     }
// }

// const fcmParam = {
//     body: {
//         fcm_token: Joi.string().required()
//     }
// }

// const usersParam = {
//     query: {
//         search: Joi.string().required().allow(''),
//         page: Joi.number().integer().required(),
//         size: Joi.number().integer().required()
//     }
// }

module.exports = {
    loginValidation,
    // createUserParam,
    // fcmParam,
    // updateUserParam,
    // usersParam
};