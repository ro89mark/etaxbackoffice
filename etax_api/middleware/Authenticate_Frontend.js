const jwt = require('jsonwebtoken');
const ModelUser = require('../models/mt_customer');

const authenticateFrontend = async (req, res, next) => {
  const authorization = req.headers['authorization'];
  if (authorization) {
    const token = authorization.replace('Bearer ', '').replace('bearer ', '');

    try {

      const decoded = jwt.verify(token, config.jwtSecret);
      if (decoded) {
        const user = await ModelUser.getAuth({
          username: decoded.sub ? decoded.sub.toLowerCase() : '',
        });

        if (user) {
          req.user = user;
          return next();
        }
        return res.status(401).send({
          error: 'Unauthorized',
          message: 'Authentication failed (token).',
        });
      }
    } catch (e) {
      console.log(e);
      // console.log('authen', e);
      // return res.status(401).send({ error: 'Unauthorized', message: e });
    }
  }

  return res
    .status(401)
    .send({ error: 'Unauthorized', message: 'Authentication failed (token).' });
};

module.exports = authenticateFrontend;
