const pathLogger = (req, res, next) => {
    if (!req.url.includes('/uploads')) {
        console.log('logging: %s %s %s', req.method, req.url, req.path);
    }
    next();
}

module.exports = pathLogger;
