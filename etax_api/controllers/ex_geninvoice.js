const models = require('../models/ex_geninvoice');
const moment = require('moment');
const options = require('../helper/options');
const fs = require('fs');
const pdf = require('pdf-creator-node');
const path = require("path");


exports.GetInvoicePayStatus = async (req, res, next) => {
    try {
        const result = await models.getInvoicePayStatus();
        return res.status(200).json({
            data: result,
        });
    } catch (error) {
        next(error);
    }
};

exports.generatePdf = async (req, res, next) => {
    try {

        const data = req.body
        const dataHtml = req.body
        const date = moment(dataHtml.date).format('DD MMMM YYYY')
        dataHtml.date = date
        const html = fs.readFileSync(path.join(__dirname, '../views/template.html'), 'utf-8');
        const filename = 'invoice' + data.id + '.pdf';
        const document = {
            html: html,
            data: {
                data: dataHtml
            },
            path: './docs/' + filename
        }
        const filepath = '/docs/' + filename;
        data.filepath = filepath

        pdf.create(document, options)
            .then(updateStatus = async (res) => {
                await models.updateFileStatus(data)
                await models.insertDataPdf(data)
                console.log(res)
            }).catch(error => {
                console.log(error);
            });
        return res.status(200).json({
            data: "generated"
        });

    } catch (error) {
        next(error);
    }
};