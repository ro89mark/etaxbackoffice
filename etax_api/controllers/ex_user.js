const models = require('../models/ex_user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const secret = 'etax-express'

exports.GetMasterUsers = async (req, res, next) => {
    try {
        const result = await models.getUsers();
        return res.status(200).json({
            data: result,
        });

    } catch (error) {
        next(error);
    }
};

exports.RegisterUser = async (req, res, next) => {
    try {
        const data = req.body
        const password = data.password
        const salt = bcrypt.genSaltSync(saltRounds);
        const hash = bcrypt.hashSync(password, salt);
        data.password = hash
        const result = await models.registerUser(data);
        return res.status(200).json({
            data: data,
        });

    } catch (error) {
        next(error);
    }
};

exports.UsersLogin = async (req, res, next) => {
    try {
        const data = req.body
        const result = await models.loginUser(data);
        if (result.rowCount === 0) {
            return res.status(400).json({
                data: "username not found",
            });
        }
        const username = result.rows[0].username
        const password = result.rows[0].password
        const role = result.rows[0].role
        const checkPassword = bcrypt.compareSync(data.password, password);
        if (checkPassword) {
            const token = jwt.sign({ username: username, role: role }, secret,{ expiresIn: '1h' });
            return res.status(200).json({
                data: "success",
                token : token,
            });
        }
        else{
            return res.status(400).json({
                data: "failed",
            });
        }
        // return res.status(200).json({
        //     data: checkPassword,
        // });

    } catch (error) {
        next(error);
    }
};

exports.AuthenLogin = async (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1]
        const decoded = jwt.verify(token, secret);
        return res.status(200).json({
            data: decoded,
        });

    } catch (error) {
        next(error);
    }
};

exports.UpdateUsers = async (req, res, next) => {
    try {
        const data = req.body
        const result = await models.updateUser(data);
        return res.status(200).json({
            data: data,
        });
    } catch (error) {
        next(error);
    }
};

exports.DeleteUsers = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await models.deleteUser(id);
        return res.status(200).json({
            data: result,
        });
    } catch (error) {
        next(error);
    }
};



