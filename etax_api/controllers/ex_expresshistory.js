const models = require('../models/ex_expresshistory');
const moment = require('moment');
var path = require("path");


exports.GetExpressHistory = async (req, res, next) => {
    try {
        const result = await models.getExpressHistory();
        return res.status(200).json({
            data: result,
        });
    } catch (error) {
        next(error);
    }
};
