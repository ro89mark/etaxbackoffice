const models = require('../models/ex_expressinvoice');
const moment = require('moment');
var path = require("path");


exports.GetInvoiceExpress = async (req, res, next) => {
    try {
        const result = await models.getInvoiceExpress();
        return res.status(200).json({
            data: result,
        });
    } catch (error) {
        next(error);
    }
};
