const models = require('../models/ex_notice');
const requestOcr = require('../helper/ocr');
const moment = require('moment');
// const request = require('request');
var request = require('request-promise')
const fs = require("fs");
var path = require("path");
const e = require('express');

// exports.InsertNotice = async (req, res, next) => {
//     try {
//         const file = req.file;
//         const { invoice_id, origin_bank, price_front } = req.body
//         console.log(file.destination)
//         var link = file.destination.replace("public/", "");
//         link = path.join(link, file.filename);
//         const setModel = {
//             payment_date: moment().format('YYYY-MM-DD HH:mm:ss'),
//             notice_price: price_front,
//             image: link,
//             invoice_id: invoice_id,
//             origin_bank: origin_bank
//         }
//         const resultModel = await models.insertNotice(setModel);
//         return res.status(200).json({
//             data: resultModel,
//         });
//     } catch (error) {
//         next(error);
//     }
// };

exports.InsertNotice = async (req, res, next) => {
    try {
        console.log("111")
        const file = req.file;
        const { invoice_id, origin_bank, price_front } = req.body
        console.log(file.destination)
        var link = file.destination.replace("public/", "");
        link = path.join(link, file.filename);

        console.log(origin_bank)
        if (origin_bank === "ธนาคารกรุงเทพ") {
            console.log("กรุงเทพ")
            const ocr = await request({
                method: "POST",
                uri: "https://api.aiforthai.in.th/ocr",
                headers: {
                    "Apikey": "LgArg8PNY2BiY1cmtFsE1XXN6bP6O903",
                    "Content-Type": "multipart/form-data"
                },
                formData: {
                    "uploadfile": fs.createReadStream(link)
                },
                json: true
            })

            const result = ocr.Original.split(/\r?\n/).filter(element => element);

            let findTotal
            for (let i = 0; i < result.length; i++) {
                if (result[i] === 'จํานวนเงิน') {
                    findTotal = result[i + 1]
                    // console.log(subtotal)
                    break
                }
            }
            const subtotal = findTotal.split(" ")
            const convertTotal = subtotal[0].replace(/\,/g, '')
            console.log(convertTotal)
            console.log(price_front)
            if (Number(convertTotal) == Number(price_front)) {
                const setModel = {
                    payment_date: moment().format('YYYY-MM-DD HH:mm:ss'),
                    notice_price: convertTotal,
                    image: link,
                    invoice_id: invoice_id,
                    origin_bank: origin_bank
                }
                console.log(setModel.invoice_id)
                const addNotice = await models.insertNotice(setModel)
                await models.updatePaystatusInvoice(setModel.invoice_id)
                return res.status(200).json({
                    data: addNotice,
                });
            }
            else if (Number(convertTotal) > Number(price_front)) {
                const setModel = {
                    payment_date: moment().format('YYYY-MM-DD HH:mm:ss'),
                    notice_price: convertTotal,
                    image: link,
                    invoice_id: invoice_id,
                    origin_bank: origin_bank
                }
                await models.insertNotice(setModel)
                await models.updatePaystatusOverInvoice(setModel.invoice_id)
                return res.status(400).json({
                    data: "ยอดโอนเงินเกิน กรุณาติดต่อรับเงินคืน",
                });
            }
            else {
                const setModel = {
                    payment_date: moment().format('YYYY-MM-DD HH:mm:ss'),
                    notice_price: convertTotal,
                    image: link,
                    invoice_id: invoice_id,
                    origin_bank: origin_bank
                }
                await models.insertNotice(setModel)
                await models.updatePaystatusUnderInvoice(setModel.invoice_id)
                return res.status(400).json({
                    data: "ยอดโอนเงินไม่ครบ กรุณาติดต่อรับเงินคืน",
                });
            }
        }
        else if (origin_bank === "ธนาคารกสิกรไทย") {
            console.log("kasikorn")
            const ocr = await request({
                method: "POST",
                uri: "https://api.aiforthai.in.th/ocr",
                headers: {
                    "Apikey": "LgArg8PNY2BiY1cmtFsE1XXN6bP6O903",
                    "Content-Type": "multipart/form-data"
                },
                formData: {
                    "uploadfile": fs.createReadStream(link)
                },
                json: true
            })
            const result = ocr.Original.split(/\r?\n/).filter(element => element);
            console.log(result)

            let findTotal
            for (let i = 0; i < result.length; i++) {
                if (result[i] === 'จํานวน:') {
                    findTotal = result[i + 1]
                    // console.log(subtotal)
                    break
                }
            }
            // console.log(findTotal)
            console.log(findTotal)
            const subtotal = findTotal.split(" ")
            const convertTotal = subtotal[0].replace(/\,/g, '')
            console.log(convertTotal)
            console.log(price_front)
            if (Number(convertTotal) == Number(price_front)) {
                const setModel = {
                    payment_date: moment().format('YYYY-MM-DD HH:mm:ss'),
                    notice_price: convertTotal,
                    image: link,
                    invoice_id: invoice_id,
                    origin_bank: origin_bank
                }
                console.log(setModel.invoice_id)
                const addNotice = await models.insertNotice(setModel)
                await models.updatePaystatusInvoice(setModel.invoice_id)
                return res.status(200).json({
                    data: addNotice,
                });
            }
            else if (Number(convertTotal) > Number(price_front)) {
                const setModel = {
                    payment_date: moment().format('YYYY-MM-DD HH:mm:ss'),
                    notice_price: convertTotal,
                    image: link,
                    invoice_id: invoice_id,
                    origin_bank: origin_bank
                }
                await models.insertNotice(setModel)
                await models.updatePaystatusOverInvoice(setModel.invoice_id)
                return res.status(400).json({
                    data: "ยอดโอนเงินเกิน กรุณาติดต่อรับเงินคืน",
                });
            }
            else {
                const setModel = {
                    payment_date: moment().format('YYYY-MM-DD HH:mm:ss'),
                    notice_price: convertTotal,
                    image: link,
                    invoice_id: invoice_id,
                    origin_bank: origin_bank
                }
                await models.insertNotice(setModel)
                await models.updatePaystatusUnderInvoice(setModel.invoice_id)
                return res.status(400).json({
                    data: "ยอดโอนเงินไม่ครบ กรุณาติดต่อรับเงินคืน",
                });
            }
        }
        else if (origin_bank === "ธนาคารไทยพาณิชย์") {
            const ocr = await request({
                method: "POST",
                uri: "https://api.aiforthai.in.th/ocr",
                headers: {
                    "Apikey": "LgArg8PNY2BiY1cmtFsE1XXN6bP6O903",
                    "Content-Type": "multipart/form-data"
                },
                formData: {
                    "uploadfile": fs.createReadStream(link)
                },
                json: true
            })
            const result = ocr.Original.split(/\r?\n/).filter(element => element);
            const indexQrCode = result.indexOf("ตรวจสอบสถานะการโอนเงิน")
            const resultTotal = result[indexQrCode - 2].split(" ")
            const convertTotal = resultTotal[resultTotal.length - 1].replace(/\,/g, '')
            console.log(convertTotal)
            if (Number(convertTotal) == Number(price_front)) {
                const setModel = {
                    payment_date: moment().format('YYYY-MM-DD HH:mm:ss'),
                    notice_price: convertTotal,
                    image: link,
                    invoice_id: invoice_id,
                    origin_bank: origin_bank
                }
                console.log(setModel.invoice_id)
                const addNotice = await models.insertNotice(setModel)
                await models.updatePaystatusInvoice(setModel.invoice_id)
                return res.status(200).json({
                    data: addNotice,
                });
            }
            else if (Number(convertTotal) > Number(price_front)) {
                const setModel = {
                    payment_date: moment().format('YYYY-MM-DD HH:mm:ss'),
                    notice_price: convertTotal,
                    image: link,
                    invoice_id: invoice_id,
                    origin_bank: origin_bank
                }
                await models.insertNotice(setModel)
                await models.updatePaystatusOverInvoice(setModel.invoice_id)
                return res.status(400).json({
                    data: "ยอดโอนเงินเกิน กรุณาติดต่อรับเงินคืน",
                });
            }
            else {
                const setModel = {
                    payment_date: moment().format('YYYY-MM-DD HH:mm:ss'),
                    notice_price: convertTotal,
                    image: link,
                    invoice_id: invoice_id,
                    origin_bank: origin_bank
                }
                await models.insertNotice(setModel)
                await models.updatePaystatusUnderInvoice(setModel.invoice_id)
                return res.status(400).json({
                    data: "ยอดโอนเงินไม่ครบ กรุณาติดต่อรับเงินคืน",
                });
            }
        }

    } catch (error) {
        next(error);
    }
};

exports.GetInvoiceByEmail = async (req, res, next) => {
    try {
        const data = req.body
        const result = await models.getInvoiceByEmail(data);
        return res.status(200).json({
            data: result,
        });
    } catch (error) {
        next(error);
    }
};

exports.GetInvoicePriceById = async (req, res, next) => {
    try {
        const { id } = req.params
        const result = await models.getInvoicePriceById(id);
        return res.status(200).json({
            data: result,
        });
    } catch (error) {
        next(error);
    }
};

// exports.testOcr = async (req, res, next) => {
//     try {
//         const file = "docs/uploadsNotice/65/กรุงเทพ.jpg"
//         const ocr = await request({
//             method: "POST",
//             uri: "https://api.aiforthai.in.th/ocr",
//             headers: {
//                 "Apikey": "LgArg8PNY2BiY1cmtFsE1XXN6bP6O903",
//                 "Content-Type": "multipart/form-data"
//             },
//             formData: {
//                 "uploadfile": fs.createReadStream(file)
//             },
//             json: true
//         })
//         const result = ocr.Original.split(/\r?\n/).filter(element => element);

//         let findTotal
//         for (let i = 0; i < result.length; i++) {
//             if (result[i] === 'จํานวนเงิน') {
//                 findTotal = result[i + 1]
//                 // console.log(subtotal)
//                 break
//             }
//         }
//         const subtotal = findTotal.split(" ")
//         console.log(subtotal[0])
//         return res.status(200).json({
//             data: "success"
//         });
//     } catch (error) {
//         next(error);
//     }
// };



