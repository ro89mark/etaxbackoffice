const models = require('../models/ex_expressinvoice');
require('dotenv').config();
const nodemailer = require('nodemailer')
const moment = require('moment');
const options = require('../helper/options');
const fs = require('fs');
const pdf = require('pdf-creator-node');
const path = require("path");
const { dirname } = require('path');


exports.sendEmail = async (req, res, next) => {
    try {
        const MY_WEB = process.env.MY_WEBSITE
        const data = req.body
        const file = `${MY_WEB}${data.file}`
        console.log(file)
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: 'etaxexpress61@gmail.com',
                pass: 'wnou nrsu phny bmhu'
            }
        })

        const mailOptions = {
            from: 'etaxexpress61@gmail.com',
            to: data.email,
            subject: "ใบกำกับภาษี",
            text: `
            บริษัท ETAX-EXPRESS
            ถึงคุณ : ${data.name} 
            คุณได้ทำการสั่งซื้อสินค้า : ${data.product_name}
            จำนวน 1 ชิ้น
            ราคารวมทั้งหมด : ${data.subtotal}
            ขอบคุณที่ซื้อสินค้ากับเรา และนี่คือใบกำกับภาษีของคุณ : ${data.invoice_num}`,
            attachments: [
                {
                    path: file
                }
            ]
        }

        transporter.sendMail(mailOptions, async (err) => {
            if (err) {
                // return console.log(err);
                return res.status(400).json({
                    data: "เกิดข้อผิดพลาดในการส่งอีเมล",
                });
            }
            await models.updateExpressStatus(data.id)
            return res.status(200).json({
                data: "sent mail succ",
            });
        });



    } catch (error) {
        next(error);
    }
};

// exports.downloadFilePdf = async (req, res, next) => {


//     const data = req.body
//     const dirname = __dirname.slice(0, 40)
//     const file = `${dirname}${data.file}`;
//     console.log(file)
//     res.download(file, (err) => {
//         console.log("1")
//         if (err) {
//             console.log(err);
//         }
//     });

// };