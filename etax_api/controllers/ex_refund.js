const models = require('../models/ex_refund');

exports.GetInvoiceRefund = async (req, res, next) => {
    try {
        const result = await models.getInvoiceRefund();
        return res.status(200).json({
            data: result,
        });
    } catch (error) {
        next(error);
    }
};

exports.UpdateOverRefund = async (req, res, next) => {
    try {
        const data = req.body
        await models.updateStatusOverRefund(data.id);
        await models.updatePriceOverRefund(data.subtotal , data.notices_id);
        return res.status(200).json({
            data: "Success",
        });
    } catch (error) {
        next(error);
    }
};

exports.UpdateUnderRefund = async (req, res, next) => {
    try {
        const data = req.body
        await models.updateStatusUnderRefund(data.id);
        await models.removeUnderRefund(data.notices_id);
        return res.status(200).json({
            data: "Success",
        });
    } catch (error) {
        next(error);
    }
};


