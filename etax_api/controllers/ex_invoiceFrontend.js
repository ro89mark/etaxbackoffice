const models = require('../models/ex_invoiceFrontend');
const invnum = require('invoice-number')

exports.InsertInvoiceFrontend = async (req, res, next) => {
    try {
        const data = req.body
        const genInvoiceNum = await models.searchInvoiceNum();
        if(genInvoiceNum.length === 0){
            data.invoice_num = "EXPRESS1"
            const result = await models.insertInvoiceFront(data);
            return res.status(200).json({
                data: result,
            });
        }
        const invoiceNumGenId = Number(genInvoiceNum[0].id) + 1
        const joinString = "EXPRESS"+invoiceNumGenId.toString()
        data.invoice_num = joinString
        const result = await models.insertInvoiceFront(data);
        return res.status(200).json({
            data: result,
        });
    } catch (error) {
        next(error);
    }
};




