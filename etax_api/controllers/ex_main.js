const models = require('../models/ex_main');

exports.GetInvoiceMain = async (req, res, next) => {
    try {
        await models.checkDuedateExpire();
        const result = await models.getInvoiceMain();
        return res.status(200).json({
            data: result,
        });
    } catch (error) {
        next(error);
    }
};

exports.DeleteInvoiceMain = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await models.deleteInvoiceMain(id);
        return res.status(200).json({
            data: result,
        });
    } catch (error) {
        next(error);
    }
};

exports.UpdateInvoiceMain = async (req, res, next) => {
    try {
        const data = req.body
        const result = await models.updateInvoiceMain(data);
        return res.status(200).json({
            data: data,
        });
    } catch (error) {
        next(error);
    }
};



