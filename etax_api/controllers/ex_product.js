const models = require('../models/ex_product');
const moment = require('moment');
var path = require("path");


exports.GetAllExproduct = async (req, res, next) => {
    try {
        const result = await models.getExproduct();
        return res.status(200).json({
            data: result,
        });
    } catch (error) {
        next(error);
    }
};

exports.GetProductById = async (req, res, next) => {
    try {
        const { id } = req.params;
        const result = await models.getExproductById(id);
        return res.status(200).json({
            data: result,
        });
    } catch (error) {
        next(error);
    }
};

// exports.InsertMtRedeem = async (req, res, next) => {
//     try {

//         const file = req.file;
//         const user_id = req.user.id
//         const { name, description, point, createby, updateby, campaign_id } = req.body;
//         var link = file.destination.replace("public/", "");
//         link = path.join(link, file.filename);

//         var setModel = {
//             name: name,
//             description: description,
//             point: point,
//             createby: user_id,
//             updateby: user_id,
//             campaign_id: campaign_id,
//             image: link,
//         };

//         const result = await models.insertMtRedeem(setModel);
//         return res.status(200).json({
//             data: setModel,
//         });
//     } catch (error) {
//         next(error);
//     }
// };

// exports.GetAllMtRedeem = async (req, res, next) => {
//     try {
//         const result = await models.getMtRedeem();
//         return res.status(200).json({
//             data: result,
//         });
//     } catch (error) {
//         next(error);
//     }
// };

// exports.GetMtRedeemById = async (req, res, next) => {
//     try {
//         const { id } = req.params
//         const result = await models.getMtRedeemById(id);
//         return res.status(200).json({
//             data: result,
//         });
//     } catch (error) {
//         next(error);
//     }
// };

// exports.GetMtRedeemByCampaignId = async (req, res, next) => {
//     try {
//         const { id } = req.params
//         const result = await models.getMtRedeemByCampaignId(id);
//         return res.status(200).json({
//             data: result,
//         });
//     } catch (error) {
//         next(error);
//     }
// };

// exports.GetMtredeemAllCampaign = async (req, res, next) => {
//     try {
//         const { id } = req.params
//         const result = await models.getMtRedeemAllCampaign();
//         return res.status(200).json({
//             data: result,
//         });
//     } catch (error) {
//         next(error);
//     }
// };

// exports.DeleteMtRedeem = async (req, res, next) => {
//     try {
//         const { id } = req.params
//         const result = await models.deleteMtRedeem(id);
//         return res.status(200).json({
//             data: id,
//         });
//     } catch (error) {
//         next(error);
//     }
// };

// exports.UpdateMtRedeem = async (req, res, next) => {
//     try {
//         const file = req.file;
//         const user_id = req.user.id
//         const { name, description, point, updateby, campaign_id, id, image } = req.body;

//         var setModel = {
//             name: name,
//             description: description,
//             point: point,
//             updateby: user_id,
//             campaign_id: campaign_id,
//             id: id,
//             image: image
//         };
//         if (file) {
//             console.log(file)
//             var link = file.destination.replace("public/", "");
//             link = path.join(link, file.filename);
//             setModel.image = link
//         }

//         const result = await models.updateMtRedeem(setModel);
//         return res.status(200).json({
//             data: setModel,
//         });
//     } catch (error) {
//         next(error);
//     }
// };
