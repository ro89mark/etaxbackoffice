const db = require('../config/db');


exports.getInvoiceRefund = async () => {
    const query = {
        text: `select invoice.id, invoice.name , invoice.invoice_num ,invoice.email,invoice.tax_id,invoice.subtotal,invoice.paystatus,notices.notice_price,notices.id as notices_id from notices
        LEFT JOIN invoice ON (notices.invoice_id = invoice.id) WHERE invoice.paystatus = 'O' or invoice.paystatus = 'U' ORDER BY invoice.id`
    };
    const { rows } = await db.query(query);
    return rows
}
exports.updateStatusOverRefund = async (id) => {
    const query = {
        text: `update invoice SET paystatus = 'A' where id = ${id} `
    };
    const result  = await db.query(query);
    return result
}

exports.updatePriceOverRefund = async (subtotal , idnotice) => {
    const query = {
        text: `update notices SET notice_price = ${subtotal} where id = ${idnotice} `
    };
    const result  = await db.query(query);
    return result
}

exports.updateStatusUnderRefund = async (id) => {
    const query = {
        text: `update invoice SET paystatus = 'I' where id = ${id} `
    };
    const result  = await db.query(query);
    return result
}

exports.removeUnderRefund = async (idnotice) => {
    const query = {
        text: `DELETE FROM notices WHERE id = ${idnotice}`
    };
    const result  = await db.query(query);
    return result
}
