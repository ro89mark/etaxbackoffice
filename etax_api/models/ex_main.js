const db = require('../config/db');

exports.getInvoiceMain  = async () => {
    const query = {
        text: `SELECT * from invoice ORDER BY id`
    };
    const  {rows}  = await db.query(query);
    return rows
}

// exports.getInvoiceMain = async () => {
//     const query = {
//         text: `select invoice.id, invoice.name , invoice.invoice_num ,invoice.email,invoice.tax_id,invoice.subtotal,invoice.paystatus,notices.notice_price from notices
//         LEFT JOIN invoice ON (notices.invoice_id = invoice.id) ORDER BY invoice.id`
//     };
//     const { rows } = await db.query(query);
//     return rows
// }

exports.checkDuedateExpire = async () => {
    const query = {
        text: `DELETE FROM invoice where paystatus = 'I' and CURRENT_DATE > duedate`
    };
    const { rows } = await db.query(query);
    return rows
}

exports.deleteInvoiceMain = async (id) => {
    const query = {
        text: `DELETE FROM invoice WHERE id = ${id} `
    };
    const result = await db.query(query);
    return result
}

// exports.updateInvoiceMain  = async (data) => {
//     const query = {
//         text: `update invoice SET  name= $1, invoice_num= $2, address= $3, province= $4, district= $5, sub_district= $6, postal_code= $7,
//          email= $8, date= $9, tax_id= $10, total= $11, taxrate= $12, paystatus= $13, filestatus= $14, subtotal= $15 where id = $16 `,
//         values: [
//             data.name,
//             data.invoice_num,
//             data.address,
//             data.province,
//             data.district,
//             data.sub_district,
//             data.postal_code,
//             data.email,
//             data.date,
//             data.tax_id,
//             data.total,
//             data.taxrate,
//             data.paystatus,
//             data.filestatus,
//             data.subtotal,
//             data.id    
//         ]
//     };
//     const  result  = await db.query(query);
//     return result
// }

exports.updateInvoiceMain = async (data) => {
    const query = {
        text: `update invoice SET name= $1, invoice_num= $2,email= $3, tax_id= $4 where id = $5 `,
        values: [
            data.name,
            data.invoice_num,
            data.email,
            data.tax_id,
            data.id
        ]
    };
    const result = await db.query(query);
    return result
}


