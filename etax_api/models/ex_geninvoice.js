const db = require('../config/db');
const moment = require('moment');

exports.getInvoicePayStatus = async () => {
    const query = {
        text: `SELECT * FROM invoice WHERE paystatus = 'A' and filestatus = 'I'`,
    };
    const { rows } = await db.query(query);
    return rows
}

exports.updateFileStatus = async (data) => {
    const query = {
        text: `update invoice SET filestatus= $1 WHERE id = $2`,
        values: [
            "A",
            data.id
        ]
    };
    const { rows } = await db.query(query);
    return rows
}

exports.insertDataPdf = async (data) => {
    const query = {
        text: `INSERT INTO filepdf(
            file,createdate,express_status,invoice_id) 
          VALUES($1,$2,$3,$4)`,
        values: [
            data.filepath,
            moment().format('YYYY-MM-DD HH:mm:ss'),
            "I",
            data.id
        ]
    };
    const { rows } = await db.query(query);
    return rows
}

// exports.insertMtRedeem = async (payload) => {
//     const query = {
//         text: `INSERT INTO mt_redeem(name, description, point,record_status, createdate, createby,updatedate,updateby,campaign_id,image) 
//           VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10) `,
//         values: [
//             payload.name,
//             payload.description,
//             payload.point,
//             'A',
//             moment().format('YYYY-MM-DD HH:mm:ss'),
//             payload.createby,
//             moment().format('YYYY-MM-DD HH:mm:ss'),
//             payload.updateby,
//             payload.campaign_id,
//             payload.image
//         ],
//     };
//     const result = await db.query(query);
//     return result
// };

// // exports.getMtRedeem = async () => {
// //     const query = {
// //         text: `SELECT * FROM mt_redeem WHERE record_status = 'A' ORDER BY id`,
// //     };
// //     const { rows } = await db.query(query);
// //     return rows
// // }
// exports.getMtRedeem = async () => {
//     const query = {
//         text: `SELECT 	b.id,
// 		b.name,
// 		b.point,
// 		b.description,
// 		b.createdate ,
// 		b.updatedate,
// 		b.campaign_id,
// 		b.image,
// 		u.userfullname as updateby,
// 		uc.userfullname as createby
// FROM mt_redeem as b
// LEFT JOIN mt_user as u ON b.updateby = u.id::varchar
// LEFT JOIN mt_user as uc ON b.createby = uc.id::varchar
// WHERE b.record_status = 'A' ORDER BY b.id`,
//     };
//     const { rows } = await db.query(query);
//     return rows
// }

// exports.getMtRedeemById = async (id) => {
//     const query = {
//         text: `SELECT * FROM mt_redeem WHERE record_status = 'A' AND id = ${id}`,
//     };
//     const { rows } = await db.query(query);
//     return rows
// }

// exports.getMtRedeemByCampaignId = async (id) => {
//     const query = {
//         text: `SELECT mt_redeem.id,mt_redeem.name,mt_redeem.description,mt_redeem.point,mt_redeem.image,
//         mt_campaign.title,mt_campaign.description as cam_description ,mt_campaign.image as cam_image
//         FROM mt_redeem
//         LEFT JOIN mt_campaign ON mt_campaign.id = mt_redeem.campaign_id
//         WHERE mt_redeem.record_status = 'A' and mt_campaign.id = ${id} and mt_campaign.record_status = 'A' ORDER BY mt_redeem.id`,
//     };
//     const { rows } = await db.query(query);
//     return rows
// }

// exports.getMtRedeemAllCampaign = async (id) => {
//     const query = {
//         text: `select mt_redeem.id , mt_redeem.name , mt_redeem.description, mt_redeem.point,mt_redeem.image from mt_redeem
//         LEFT JOIN mt_campaign ON (mt_redeem.campaign_id = mt_campaign.id)
//         where DATE(mt_campaign.end_date) >= DATE(NOW()) and DATE(NOW()) >= DATE(mt_campaign.start_date) 
//         and mt_redeem.record_status = 'A' and mt_campaign.record_status = 'A'`,
//     };
//     const { rows } = await db.query(query);
//     return rows
// }

// exports.deleteMtRedeem = async (id) => {
//     const query = {
//         text: `UPDATE mt_redeem SET record_status = $1 WHERE id = $2`,
//         values: [
//             "I",
//             id
//         ],
//     };
//     const result = await db.query(query);
//     return result
// };

// exports.updateMtRedeem = async (payload) => {

//     const query = {
//         text: `UPDATE mt_redeem SET name = $1, description = $2, point = $3, updatedate = $4, updateby = $5 ,campaign_id = $6, image = $7
//                 WHERE id = $8
//         `,
//         values: [
//             payload.name,
//             payload.description,
//             payload.point,
//             moment().format('YYYY-MM-DD HH:mm:ss'),
//             payload.updateby,
//             payload.campaign_id,
//             payload.image,
//             payload.id
//         ]
//     };
//     const result = await db.query(query);
//     return result
// }