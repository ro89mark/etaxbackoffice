const db = require('../config/db');
const moment = require('moment');

exports.getUsers = async () => {
    const query = {
        text: `SELECT id,firstname,lastname,username,role,createdate from users  ORDER BY id`
    };
    const { rows } = await db.query(query);
    return rows
}

exports.registerUser = async (payload) => {
    const query = {
        text: `INSERT INTO users(firstname,lastname,username,password,role,createdate,updatedate) 
            VALUES($1,$2,$3,$4,$5,$6,$7) `,
        values: [
            payload.firstname,
            payload.lastname,
            payload.username,
            payload.password,
            payload.role,
            moment().format('YYYY-MM-DD HH:mm:ss'),
            moment().format('YYYY-MM-DD HH:mm:ss'),
        ],
    };
    const result = await db.query(query);
    return result
}

exports.loginUser = async (payload) => {
    const query = {
        text: `SELECT * from users WHERE username = $1`,
        values: [
            payload.username,
        ],
    };
    const result = await db.query(query);
    return result
}

exports.updateUser = async (data) => {
    const query = {
        text: `update users SET firstname= $1, lastname= $2, username= $3, role= $4, updatedate= $5 where id = $6 `,
        values: [
            data.firstname,
            data.lastname,
            data.username,
            data.role,
            moment().format('YYYY-MM-DD HH:mm:ss'),
            data.id    
        ]
    };
    const  result  = await db.query(query);
    return result
}

exports.deleteUser  = async (id) => {
    const query = {
        text: `DELETE FROM users WHERE id = ${id} `
    };
    const  result  = await db.query(query);
    return result
}


