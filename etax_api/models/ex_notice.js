const db = require('../config/db');

exports.insertNotice = async (payload) => {
    const query = {
        text: `INSERT INTO notices(payment_date,notice_price,image,invoice_id,origin_bank) 
            VALUES($1,$2,$3,$4,$5) `,
        values: [
            payload.payment_date,
            payload.notice_price,
            payload.image,
            payload.invoice_id,
            payload.origin_bank,
        ],
    };
    const result = await db.query(query);
    return result
}

exports.getInvoiceByEmail  = async (data) => {
    const query = {
        text: `SELECT id,invoice_num from invoice WHERE email = $1 and paystatus = 'I' ORDER BY id`,
        values: [
            data.email,
        ]
    };
    const  {rows}  = await db.query(query);
    return rows
}

exports.getInvoicePriceById  = async (id) => {
    const query = {
        text: `SELECT id,subtotal from invoice WHERE id = ${id}`
    };
    const  {rows}  = await db.query(query);
    return rows
}

exports.updatePaystatusInvoice  = async (invoiceId) => {
    const query = {
        text: `UPDATE invoice SET paystatus = 'A' WHERE id = ${invoiceId}`
    };
    const  {rows}  = await db.query(query);
    return rows
}

exports.updatePaystatusOverInvoice  = async (invoiceId) => {
    const query = {
        text: `UPDATE invoice SET paystatus = 'O' WHERE id = ${invoiceId}`
    };
    const  {rows}  = await db.query(query);
    return rows
}

exports.updatePaystatusUnderInvoice  = async (invoiceId) => {
    const query = {
        text: `UPDATE invoice SET paystatus = 'U' WHERE id = ${invoiceId}`
    };
    const  {rows}  = await db.query(query);
    return rows
}

