const db = require('../config/db');
const moment = require('moment');

exports.insertInvoiceFront = async (data) => {
    const query = {
        text: `INSERT INTO invoice(
            name, invoice_num, address, province, district, sub_district, postal_code, email, date, tax_id, total, taxrate, paystatus, filestatus, subtotal,product_name,duedate) 
          VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17) `,
        values: [
            data.name,
            data.invoice_num,
            data.address,
            data.province,
            data.district,
            data.sub_district,
            data.postal_code,
            data.email,
            moment().format('YYYY-MM-DD HH:mm:ss'),
            data.tax_id,
            data.total,
            data.taxrate,
            'I',
            'I',
            data.subtotal,
            data.product_name,
            moment().add(7, 'd').format('YYYY-MM-DD HH:mm:ss')
        ],
    };
    const result = await db.query(query);
    return result
};

exports.searchInvoiceNum = async (data) => {
    const query = {
        text: `SELECT * FROM invoice ORDER BY ID DESC LIMIT 1`
    };
    const { rows } = await db.query(query);
    return rows
};