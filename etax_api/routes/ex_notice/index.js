const express = require("express");
const router = express.Router();
const ex_notice = require("../../controllers/ex_notice");
const fs = require("fs");
const multer = require("multer");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        var { invoice_id } = req.body;
        const path = `./docs/uploadsNotice/${invoice_id}`;

        if (fs.existsSync(path)) {
            fs.rmSync(path, { recursive: true, force: true });
        }
        fs.mkdirSync(path, { recursive: true });

        cb(null, path);
    },
    filename: (req, file, cb) => {
        cb(null, file.originalname);
    },
});

const upload = multer({ storage: storage });

router.route("/insertNotice").post(upload.single("file"),ex_notice.InsertNotice);
router.route("/getInvoiceByEmail").post(ex_notice.GetInvoiceByEmail);
router.route("/getInvoicePriceById/:id").post(ex_notice.GetInvoicePriceById);
// router.route("/testOcr").post(ex_notice.testOcr);


// router.route("/getInvoiceMain").get(ex_main.GetInvoiceMain);
// router.route("/deleteInvoiceMain/:id").delete(ex_main.DeleteInvoiceMain);
// router.route("/updateInvoiceMain").put(ex_main.UpdateInvoiceMain);

module.exports = router;
