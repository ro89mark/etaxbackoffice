const express = require("express");
const router = express.Router();
const authenticate = require('../../middleware/Authenticate');
const ex_user = require("../../controllers/ex_user");
const fs = require("fs");

router.route("/getMasterUsers").get(ex_user.GetMasterUsers);
router.route("/register").post(ex_user.RegisterUser);
router.route("/login").post(ex_user.UsersLogin);
router.route("/authen").post(ex_user.AuthenLogin);
router.route("/updateUsers").put(ex_user.UpdateUsers);
router.route("/deleteUsers/:id").delete(ex_user.DeleteUsers);

module.exports = router;