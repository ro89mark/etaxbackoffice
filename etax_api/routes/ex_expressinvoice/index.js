const express = require("express");
const router = express.Router();
const authenticate = require('../../middleware/Authenticate');
const ex_expressinvoice = require("../../controllers/ex_expressinvoice");
const fs = require("fs");

router.route("/getInvoiceExpress").get(ex_expressinvoice.GetInvoiceExpress);

module.exports = router;