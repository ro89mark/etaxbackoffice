const express = require("express");
const router = express.Router();
const authenticate = require('../../middleware/Authenticate');
const ex_invoice = require("../../controllers/ex_geninvoice");
const fs = require("fs");

router.route("/getInvoiceForGen").get(ex_invoice.GetInvoicePayStatus);
// router.route("/generateInvoice").post(ex_invoice.generatePdf);
router.route("/generateInvoice").post(ex_invoice.generatePdf);

module.exports = router;