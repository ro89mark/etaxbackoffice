const express = require("express");
const router = express.Router();
const authenticate = require('../../middleware/Authenticate');
const ex_downloadfile = require("../../controllers/ex_downloadfile");
const fs = require("fs");

router.route("/downloadfilepdf").post(ex_downloadfile.downloadFilePdf);

module.exports = router;