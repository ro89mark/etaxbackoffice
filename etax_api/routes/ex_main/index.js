const express = require("express");
const router = express.Router();
const ex_main = require("../../controllers/ex_main");


router.route("/getInvoiceMain").get(ex_main.GetInvoiceMain);
router.route("/deleteInvoiceMain/:id").delete(ex_main.DeleteInvoiceMain);
router.route("/updateInvoiceMain").put(ex_main.UpdateInvoiceMain);

module.exports = router;
