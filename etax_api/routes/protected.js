const express = require('express');
const authenticate = require('../middleware/Authenticate');
const protected = require('../controllers/file_protected');

const router = express.Router();

router.route('/*').get(authenticate, protected.find);

module.exports = router;
