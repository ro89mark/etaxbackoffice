const express = require("express");
const router = express.Router();
const authenticate = require('../../middleware/Authenticate');
const ex_sendEmail = require("../../controllers/ex_sendEmail");
const fs = require("fs");

router.route("/sendmail").post(ex_sendEmail.sendEmail);

module.exports = router;