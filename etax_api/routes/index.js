const express = require("express");
// const mt_user = require("./mt_user");

// const mt_email_template = require("./mt_email_template");
// const mt_role = require('./mt_role');
// const mt_branch = require('./mt_branch');
// const mt_team = require('./mt_team');
// const mt_user_team = require('./mt_user_team');
// const mt_user_role = require('./mt_user_role');
// const mt_menu = require('./mt_menu');
// const mt_config = require("./mt_config");
// const tran_log_front = require("./tran_log_front");
// const tran_log_back = require("./tran_log_back");
// const tran_redeem = require("./tran_redeem");
// const mt_tag = require("./mt_tag");
// const mt_value = require("./mt_value");
// const mt_category = require("./mt_category");
// const mt_banner = require("./mt_banner");
// const mt_promotion = require("./mt_promotion");
// const mt_product = require("./mt_product");
// const mt_product_front = require("./mt_product_front");
// const tran_stock = require("./tran_stock");
// const mt_campaign = require("./mt_campaign");
// const mt_customer = require("./mt_customer");
// const mt_ranking = require("./mt_ranking");
// const tran_order_detail = require("./tran_order_detail");
// const tran_order = require("./tran_order");
// const verify_card = require('./verifycard');
// const tran_verify_card = require('./tran_verify_card');
// const mt_group_ps_permission = require('./mt_group_ps_permission');
// const dopa = require('./dopa');
const ex_product = require("./ex_product");
const ex_geninvoice = require("./ex_geninvoice");
const ex_expressinvoice = require("./ex_expressinvoice");
const ex_main = require("./ex_main");
const ex_invoiceFrontend = require("./ex_invoiceFrontend");
const ex_expresshistory = require("./ex_expresshistory");
const ex_download = require("./ex_download");
const ex_sendEmail = require("./ex_sendEmail")
const ex_user = require("./ex_user")
const ex_notice = require("./ex_notice")
const ex_refund = require("./ex_refund")

const router = express.Router();

// router.use("/", mt_user);
// router.use("/", mt_email_template);
// router.use("/", tran_redeem);
// router.use("/", mt_promotion);
// router.use("/", tran_stock);
// router.use("/", mt_banner);
// router.use("/", mt_campaign);
// router.use('/', mt_role);
// router.use('/', mt_branch);
// router.use('/', mt_team);
// router.use('/', mt_menu);
// router.use("/", mt_config);
// router.use("/", mt_tag);
// router.use("/", mt_value);
// router.use("/", mt_category);
// router.use('/', mt_user_team);
// router.use('/', mt_user_role);
// router.use("/", tran_log_front);
// router.use("/", tran_log_back);
// router.use("/", tran_order_detail);
// router.use("/", tran_order);
// router.use("/", mt_product);
// router.use("/", mt_product_front);
// router.use("/", mt_customer);
// router.use("/", mt_ranking);
// router.use('/verify_card', verify_card);
// router.use('/', tran_verify_card);
// router.use('/', mt_group_ps_permission);
// router.use('/dopa', dopa);

router.use("/", ex_product);
router.use("/", ex_geninvoice);
router.use("/", ex_expressinvoice);
router.use("/", ex_main);
router.use("/", ex_invoiceFrontend);
router.use("/", ex_expresshistory);
router.use("/", ex_download);
router.use("/", ex_sendEmail);
router.use("/", ex_user);
router.use("/", ex_notice);
router.use("/", ex_refund);

module.exports = router;
