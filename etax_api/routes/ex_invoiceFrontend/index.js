const express = require("express");
const router = express.Router();
const ex_invoiceFrontend = require("../../controllers/ex_invoiceFrontend");

router.route("/InsertInvoiceFrontend").post(ex_invoiceFrontend.InsertInvoiceFrontend);

module.exports = router;