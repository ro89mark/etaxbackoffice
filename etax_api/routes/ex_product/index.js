const express = require("express");
const router = express.Router();
const authenticate = require('../../middleware/Authenticate');
const ex_product = require("../../controllers/ex_product");
const fs = require("fs");

// const multer = require("multer");
// const storage = multer.diskStorage({
//     destination: (req, file, cb) => {
//         var { name } = req.body;
//         const path = `./public/uploads/mtredeem/${name}`;

//         if (fs.existsSync(path)) {
//             fs.rmSync(path, { recursive: true, force: true });
//         }
//         fs.mkdirSync(path, { recursive: true });

//         cb(null, path);
//     },
//     filename: (req, file, cb) => {
//         cb(null, file.originalname);
//     },
// });

// const upload = multer({ storage: storage });

// router.route("/mtredeem").get(mt_redeem.GetAllMtRedeem);
// router.route("/getMtredeemById/:id").get(mt_redeem.GetMtRedeemById);
// router.route("/getMtredeemByCampaignId/:id").get(mt_redeem.GetMtRedeemByCampaignId);
// router.route("/getMtredeemAllCampaign").get(mt_redeem.GetMtredeemAllCampaign);

// router.route("/mtredeem/:id").delete(mt_redeem.DeleteMtRedeem);
// router
//     .route("/mtredeem")
//     .post(authenticate,upload.single("file"), mt_redeem.InsertMtRedeem);
// router
//     .route("/mtredeem")
//     .put(authenticate,upload.single("file"), mt_redeem.UpdateMtRedeem);

router.route("/ex_product").get(ex_product.GetAllExproduct);
router.route("/ex_productbyId/:id").get(ex_product.GetProductById);

module.exports = router;