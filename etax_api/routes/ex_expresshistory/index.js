const express = require("express");
const router = express.Router();
const authenticate = require('../../middleware/Authenticate');
const ex_expresshistory = require("../../controllers/ex_expresshistory");
const fs = require("fs");

router.route("/getExpressHistory").get(ex_expresshistory.GetExpressHistory);

module.exports = router;