const express = require("express");
const router = express.Router();
const ex_refund = require("../../controllers/ex_refund");


router.route("/getInvoiceRefund").get(ex_refund.GetInvoiceRefund);
router.route("/updateOverRefund").post(ex_refund.UpdateOverRefund);
router.route("/updateUnderRefund").post(ex_refund.UpdateUnderRefund);

module.exports = router;
